
## During the week of the 7drl project

No design or code contributions will be accepted.

## Afterwards

Subsequent to the initial 7drl release, bug fixes will be accepted under the
same licence terms that the game is published under.

