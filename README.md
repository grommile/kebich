Kebich (IPA: /kɛ'bɪx/) is a seven-day roguelike created in the week preceding
the 2016 Seven-Day Roguelike Challenge. It applies a thin veneer of altered
thematic space over a relatively traditional and simplistic roguelike game
design.

## Installation

To build the game executable, run `make all` in the base directory of the
source tree.

Optionally, run `make install` as root to create a system-wide installation,
though this isn't necessary as the game doesn't load any static data at
run time.

## Dependencies

* GCC 4.x or later
* A non-antediluvian version of GNU Make
* ncurses 5.x or later

