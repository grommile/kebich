# Makefile for Kebich

# Copyright © 2005, 2016 Martin Read

# These will be moved out into an includable fragment when the configure script
# happens
srcdir::=.
prefix::=/usr/local
gamesdir::=$(prefix)/games

FEATURE_DEFS::=
# Optional features
compressed_saves::=yes

ifeq ($(compressed_saves),yes)
FEATURE_DEFS+=-DCOMPRESSED_SAVES
endif
vpath %.o .
vpath %.h $(srcdir)/src

OBJS::=u.o permobj.o permons.o display.o main.o map.o objects.o monsters.o combat.o obj2.o prng.o ai.o geometry.o
GAME::=kebich
INCS::=$(shell pkg-config --cflags-only-I panel ncurses) -I$(srcdir)/src
TRUE_CFLAGS::=$(CFLAGS) -g -O2 -Wall -Wextra -Winit-self -Wformat=2 -Wstrict-prototypes -Wwrite-strings -std=gnu11 -Wmissing-prototypes -Werror -fPIE -fpie -fstack-protector-strong -D_FORTIFY_SOURCE=2 $(FEATURE_DEFS) $(INCS)
LINKFLAGS::=-std=gnu11 -fPIE -fpie -fstack-protector-strong $(shell pkg-config --libs panel ncurses) -O2 -g -Wl,-z,relro

.PHONY: all archive clean install

all: $(GAME)

$(GAME): $(OBJS)
	$(CC) $(OBJS) $(LINKFLAGS) -o $(GAME)

install: all
	install --mode=0755 -D $(GAME) $(DESTDIR)$(gamesdir)/$(GAME)

archive:
	mkdir -p archivebuild/kebich-1.0
	cp -R Makefile README.md LICENSE src archivebuild/$(GAME)-1.0
	(cd archivebuild && tar cvzf $(GAME)_1.0.orig.tar.gz $(GAME)-1.0)

clean:
	-rm -f *.o $(GAME)

%.o: $(srcdir)/src/%.c
	$(CC) $(TRUE_CFLAGS) -c $< -o $@

ai.o: $(srcdir)/src/ai.c kebich.h monfun.h

combat.o: $(srcdir)/src/combat.c kebich.h objfun.h pobjfun.h monfun.h pmonfun.h ufun.h

display.o: $(srcdir)/src/display.c kebich.h objfun.h objfun.h pobjfun.h

geometry.o: $(srcdir)/src/geometry.c kebich.h

main.o: $(srcdir)/src/main.c kebich.h objfun.h monfun.h objfun.h pobjfun.h ufun.h

map.o: $(srcdir)/src/map.c kebich.h objfun.h monfun.h objfun.h pobjfun.h mapfun.h ufun.h

monsters.o: $(srcdir)/src/monsters.c kebich.h objfun.h pobjfun.h monfun.h pmonfun.h ufun.h

obj2.o: $(srcdir)/src/obj2.c kebich.h objfun.h objfun.h pobjfun.h ufun.h

objects.o: $(srcdir)/src/objects.c kebich.h objfun.h monfun.h pmonfun.h pobjfun.h ufun.h

permobj.o: $(srcdir)/src/permobj.c kebich.h

permons.o: $(srcdir)/src/permons.c kebich.h

prng.o: $(srcdir)/src/prng.c prng.h

u.o: $(srcdir)/src/u.c kebich.h ufun.h objfun.h

