/*! \file pobjfun.h
 * \brief permobj functions header file for Kebich
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_pobjfun_h
#define INC_pobjfun_h

#include "kebich.h"

extern void identify_pobj(int po);
extern int get_pobj_by_name(char const *name);

/*! \brief predicate: is this type of permobj known to the PC? */
static inline bool po_is_identified(int po)
{
    return permobj_stats[po].identified || (permobjs[po].flags[0] & POF0_easyknown);
}

/*! \brief predicate: can instances of this permobj be stacked? */
static inline bool po_is_stackable(int po)
{
    return (bool) (permobjs[po].flags[0] & POF0_stackable);
}

#endif

/* pobjfun.h */
