/*! \file permobj.c
 * \brief "permobj" handling for Kebich
 *
 * Permobjs contain the prototypical properties for objects.
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "kebich.h"

int last_populated_permobj = MAX_PERMOBJ_SLOTS - 1;

#define WEAP_STATS(dmg,secdmg,dtyp,rate,acy) { (dmg), (secdmg), (dtyp), (rate), 0, 0, 0, (acy), EFFECT_nil, 0 }
#define ARMR_STATS(shi,reg,eva,acy) { 0, 0, DT_PHYS, 0, (shi), (reg), (eva), (acy), EFFECT_nil, 0 }
#define SHIELDREST_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_restore_shield, (pot) }
#define SHIELDBOOST_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_boost_shield, (pot) }
#define MONSENSE_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_monster_sense, (pot) }
#define HEAL_STATS(pot) { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_restore_health, (pot) }
#define USELESS_STATS { 0, 0, DT_PHYS, 0, 0, 0, 0, 0, EFFECT_nil, 0 }

/*! \brief the object database */
Permobj const permobjs[MAX_PERMOBJ_SLOTS] = {
    {
        true, "replica katana", "replica katana", ')',
        "A cheap, low quality imitation of a traditional Japanese sword.",
        POCLASS_WEAPON, 0, 99, 100,
        WEAP_STATS(2, 0, DT_PHYS, 1, 0),
        { POF0_easyknown, 0 }
    },
    {
        true, "spoon", "spoons", ')',
        "This spoon is too big to use as a spoon. You could probably hit people with it.",
        POCLASS_WEAPON, 0, 25, 10,
        WEAP_STATS(4, 0, DT_PHYS, 1, 0),
        { POF0_easyknown, 0 }
    },
    {
        true, "baton", "batons", ')',
        "A sixty-centimetre rod of some kind of high-density polymer, with a side handle.",
        POCLASS_WEAPON, 2, 30, 20,
        WEAP_STATS(6, 0, DT_PHYS, 1, 0),
        { POF0_easyknown, 0 }
    },
    {
        true, "shock baton", "shock batons", ')',
        "A sixty-centimetre rod with an internal power supply that allows it to deliver electric shocks to its victims.",
        POCLASS_WEAPON, 5, 30, 500,
        WEAP_STATS(6, 6, DT_ELEC, 1, 0),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "phase blade", "phase blades", ')',
        "A smallish sword with a strangely shimmering blade powered by technology you don't really understand.",
        POCLASS_WEAPON, 10, 80, 10000,
        WEAP_STATS(16, 0, DT_PHYS, 1, 0),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "throwing disc", "throwing discs", '(',
        "A razor-sharp disc tailored to be thrown by hand, with some rather clever technological trickery allowing it to return to your hand after you throw it.",
        POCLASS_WEAPON, 1, 45, 300,
        WEAP_STATS(4, 0, DT_PHYS, 1, 0),
        { POF0_easyknown | POF0_ranged | POF0_advanced, 0 }
    },
    {
        true, "handgun", "handguns", '(',
        "A semi-automatic pistol chambered for a mass-produced military calibre.",
        POCLASS_WEAPON, 3, 45, 400,
        WEAP_STATS(6, 0, DT_PHYS, 1, 0),
        { POF0_easyknown | POF0_ranged, 0 }
    },
    {
        true, "blaster", "blasters", '(',
        "A hand-held directed energy weapon.",
        POCLASS_WEAPON, 6, 70, 1250,
        WEAP_STATS(8, 0, DT_PHYS, 1, 0),
        { POF0_easyknown | POF0_ranged | POF0_advanced, 0 }
    },
    {
        true, "pulse blaster", "pulse blasters", '(',
        "A rapid-firing directed energy weapon.",
        POCLASS_WEAPON, 9, 70, 10000,
        WEAP_STATS(8, 0, DT_PHYS, 3, 0),
        { POF0_easyknown | POF0_ranged, 0 }
    },
    {
        true, "1990s trenchcoat", "1990s trenchcoats", '[',
        "This leather coat is cut in a style that was the height of fashion among 1990s edgelords.",
        POCLASS_ARMOUR, 0, 99, 500,
        ARMR_STATS(0, 0, 0, 0),
        { POF0_easyknown, 0 }
    },
    {
        true, "barrier jacket", "barrier jackets", '[',
        "A bolero jacket with integrated force field enhancement systems.",
        POCLASS_ARMOUR, 3, 30, 2000,
        ARMR_STATS(50, 0, 0, 0),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "displacer harness", "displacer harnesses", '[',
        "A complicated arrangement of straps and force field projectors, causing a significant fraction of attacks to miss completely.",
        POCLASS_ARMOUR, 6, 70, 10000,
        ARMR_STATS(50, 0, 25, 0),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "modular opticore", "modular opticores", '=',
        "A plug-in module that improves your vision and steadies your aim.",
        POCLASS_PLUGIN, 2, 70, 2000,
        { 0, 0, DT_PHYS, 0, 0, 0, 0, 25, EFFECT_nil, 0 },
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "shield amplifier", "shield amplifiers", '=',
        "A plug-in module that increases the rate at which your shields regenerate.",
        POCLASS_PLUGIN, 4, 70, 3000,
        { 0, 0, DT_PHYS, 0, 0, 2, 0, 0, EFFECT_nil, 0 },
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "bottle of beer", "bottles of beer", '!',
        "Fermented grain flavoured with bitter herbs. Ingesting it might make you feel better about your injuries.",
        POCLASS_CONSUMABLE, 0, 20, 10,
        HEAL_STATS(10),
        { POF0_easyknown, 0 }
    },
    {
        true, "small medikit", "small medikits", '!',
        "A one-use packet of medical supplies, good for treating moderate injuries.",
        POCLASS_CONSUMABLE, 1, 30, 250,
        HEAL_STATS(40),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "large medikit", "large medikits", '!',
        "A one-use packet of medical supplies, good for treating even quite severe injuries.",
        POCLASS_CONSUMABLE, 5, 50, 1000,
        HEAL_STATS(60),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "shield recharger", "shield rechargers", '!',
        "A one-use battery pack that will restore 50% of your shield strength.",
        POCLASS_CONSUMABLE, 5, 50, 1000,
        SHIELDREST_STATS(50),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "shield booster", "shield boosters", '!',
        "A highly illegal one-use battery pack that can temporarily boost your shields past their rated limits. The excess shield energy will dissipate over time even if you are not taking damage.",
        POCLASS_CONSUMABLE, 5, 50, 1000,
        SHIELDBOOST_STATS(100),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "empathy injector", "empathy injectors", '!',
        "Owning the cocktail of drugs in this device is officially a capital offence. Using them will temporarily elevate your awareness to the point that you can detect nearby enemies even through solid walls.",
        POCLASS_CONSUMABLE, 10, 100, 1000,
        MONSENSE_STATS(100),
        { POF0_easyknown | POF0_advanced, 0 }
    },
    {
        true, "glamour magazine", "glamour magazines", '?',
        "A paper magazine full of images of skimpily dressed attractive humans in sexually provocative poses.",
        POCLASS_SHINIES, 1, 50, 5,
        USELESS_STATS,
        { POF0_easyknown | POF0_stackable, 0 }
    },
    {
        true, "bondage magazine", "bondage magazines", '?',
        "A paper magazine full of physically restrained attractive humans.",
        POCLASS_SHINIES, 1, 50, 12,
        USELESS_STATS,
        { POF0_easyknown | POF0_stackable, 0 }
    },
    {
        true, "fashion magazine", "fashion magazines", '?',
        "A paper magazine showcasing the stylistic excesses of the human clothing industry.",
        POCLASS_SHINIES, 1, 50, 1,
        USELESS_STATS,
        { POF0_easyknown | POF0_stackable, 0 }
    },
    {
        true, "dollar", "dollars", '$',
        "Partially transparent slips of polymer film denominated in the local fiat currency.",
        POCLASS_SHINIES, 1, 50, 1,
        USELESS_STATS,
        { POF0_easyknown | POF0_currency, 0 }
    },
};

Permobj_stat permobj_stats[MAX_PERMOBJ_SLOTS];

