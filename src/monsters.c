/*! \file monsters.c
 * \brief monster-related functionality for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define MONSTERS_C
#include "kebich.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "ufun.h"
#include <stdlib.h>
#include <string.h>

static I32list pmonlut[256];

static void pmonlut_register(int pm)
{
    uint8_t hash = string_bytehash(permons[pm].name_en);
    ++pmonlut[hash].len;
    pmonlut[hash].list = realloc(pmonlut[hash].list, pmonlut[hash].len * sizeof (int32_t));
    pmonlut[hash].list[pmonlut[hash].len - 1] = pm;
}

Mon monsters[MAX_MONS_IN_PLAY];
static int reject_mon(int pm);

void summoning(int y, int x, int how_many)
{
    int i;
    int dy;
    int dx;
    int tryct;
    int mon;
    for (i = 0; i < how_many; i++)
    {
        for (tryct = 0; tryct < 20; tryct++)
        {
            dy = zero_die(3) - 1;
            dx = zero_die(3) - 1;
            if (terrain[y + dy][x + dx] == FLOOR)
            {
                mon = create_mon(NO_PMON, y + dy, x + dx);
                if (mon != NO_MON)
                {
                    break;
                }
            }
        }
    }
}

int ood(int power, int ratio)
{
    return (power - depth + ratio - 1) / ratio;
}

int get_random_pmon(void)
{
    int tryct;
    int pm;
    for (tryct = 0; tryct < 200; tryct++)
    {
        pm = zero_die(last_populated_permon + 1);
        if (reject_mon(pm))
        {
            pm = NO_PMON;
            continue;
        }
        break;
    }
    return pm;
}

int create_mon(int pm_idx, int y, int x)
{
    int mon;
    if (pm_idx == NO_PMON)
    {
        pm_idx = get_random_pmon();
        if (pm_idx == NO_PMON)
        {
            return NO_MON;
        }
    }
    for (mon = 0; mon < MAX_MONS_IN_PLAY; mon++)
    {
        if (!monsters[mon].used)
        {
            monsters[mon].mon_id = pm_idx;
            monsters[mon].used = true;
            monsters[mon].y = y;
            monsters[mon].x = x;
            monsters[mon].hpmax = permons[pm_idx].hp + ood(permons[pm_idx].power, 1);
            monsters[mon].hpcur = monsters[mon].hpmax;
            monsters[mon].tohit = permons[pm_idx].tohit;
            monsters[mon].evasion = permons[pm_idx].evasion;
            monsters[mon].dam = permons[pm_idx].dam;
            monsters[mon].awake = false;
            mapmonster[y][x] = mon;
            permon_stats[pm_idx].num_generated++;
            return mon;
        }
    }
    return NO_MON;
}

void death_drop(int mon)
{
    int pm = monsters[mon].mon_id;
    int y = monsters[mon].y;
    int x = monsters[mon].x;
    int dy, dx;
    int tryct;
    for (tryct = 0; (tryct < 100) && ((mapobject[y][x] != NO_OBJ) || (terrain[y][x] != FLOOR)); ++tryct)
    {
        dy = zero_die(3) - 1;
        dx = zero_die(3) - 1;
        tryct++;
        y += dy;
        x += dx;
    }
    if (tryct < 100)
    {
        switch (pm)
        {
        default:
            break;
        }
    }
}

bool mon_can_pass(int mon, int y, int x)
{
    if (pos_is_nowhere(y, x) || pos_is_oob(y, x))
    {
        return false;
    }
    if (mapmonster[y][x] != NO_MON)
    {
        return false;
    }
    if ((y == u.y) && (x == u.x))
    {
        /* Sanity check! */
        return false;
    }
    if (terrain[y][x] == WALL)
    {
        return (permons[monsters[mon].mon_id].flags[0] & PMF0_wallwalk);
    }
    return true;
}

void print_mon_name(int mon, Article article)
{
    if (permons[monsters[mon].mon_id].name_en[0] == '\0')
    {
        print_msg("GROB THE VOID (%d)", monsters[mon].mon_id);
    }
    switch (article)
    {
    case Art_indef: /* a */
        print_msg("a %s", permons[monsters[mon].mon_id].name_en);
        break;
    case Art_def: /* the */
        print_msg("the %s", permons[monsters[mon].mon_id].name_en);
        break;
    case Art_indef_cap: /* A */
        print_msg("A %s", permons[monsters[mon].mon_id].name_en);
        break;
    case Art_def_cap: /* The */
        print_msg("The %s", permons[monsters[mon].mon_id].name_en);
        break;
    }
}

void damage_mon(int mon, int amount, bool by_you, int killer)
{
    Mon *mptr;
    mptr = monsters + mon;
    if (amount >= mptr->hpcur)
    {
        if (by_you)
        {
            permon_stats[mptr->mon_id].killed_by_player++;
            notify_ukillm(mon);
        }
        else
        {
            permon_stats[mptr->mon_id].killed_by_other++;
            notify_mkillm(mon, killer);
        }
        death_drop(mon);
        mapmonster[mptr->y][mptr->x] = NO_MON;
        mptr->used = false;
        map_updated = true;
        display_update();
    }
    else
    {
        mptr->hpcur -= amount;
    }
}

int pmon_is_undead(int pm)
{
    return (permons[pm].flags[0] & PMF0_undead);
}

int pmon_resists_cold(int pm)
{
    switch (pm)
    {
    default:
        return 0;
    }
}

int pmon_resists_fire(int pm)
{
    switch (pm)
    {
    default:
        return 0;
    }
}

int reject_mon(int pm)
{
    if ((permons[pm].power > depth) || (percentile() < permons[pm].rarity))
    {
        return 1;
    }
    return 0;
}

void move_mon(int mon, int y, int x)
{
    Mon *mptr;
    mptr = monsters + mon;
    mapmonster[mptr->y][mptr->x] = NO_MON;
    mptr->y = y;
    mptr->x = x;
    mapmonster[mptr->y][mptr->x] = mon;
    map_updated = 1;
    display_update();
}

void mon_acts(int mon)
{
    Mon *mptr;
    int pm;
    int dy, dx;
    int y, x;
    int sy, sx;
    mptr = monsters + mon;
    pm = mptr->mon_id;
    /* dy,dx == direction monster must go to reach you. */
    dy = u.y - mptr->y;
    dx = u.x - mptr->x;
    y = mptr->y;
    x = mptr->x;
    if ((-2 < dy) && (dy < 2) && (-2 < dx) && (dx < 2))
    {
        mhitu(mon);
    }
    else if ((roomnums[u.y][u.x] != NO_ROOM) && (roomnums[u.y][u.x] == roomnums[mptr->y][mptr->x]))
    {
        /* In same room. */
        if (!mptr->awake)
        {
            print_mon_name(mon, 2);
            print_msg(" notices you.\n");
            mptr->awake = true;
        }
        select_space(&y, &x, u.y, u.x, permons[pm].ai);
        move_mon(mon, y, x);
    }
    else if (roomnums[y][x] == NO_ROOM)
    {
        /* In a corridor. */
        sy = dy ? ((dy > 0) ? 1 : -1) : 0;
        sx = dx ? ((dx > 0) ? 1 : -1) : 0;
        if (sy && ((terrain[y + sy][x] == FLOOR) && (mapmonster[y + sy][x] == NO_MON)))
        {
            move_mon(mon, y + sy, x);
        }
        else if (sx && ((terrain[y][x + sx] == FLOOR) && (mapmonster[y][x + sx] == NO_MON)))
        {
            move_mon(mon, y, x + sx);
        }
    }
    else
    {
        /* Different room. For now, do nothing. */
    }
}

int get_pmon_by_name(char const *name_en)
{
    uint8_t hash = string_bytehash(name_en);
    int i;
    if (pmonlut[hash].len == 0)
    {
        return NO_PMON;
    }
    for (i = 0; i < pmonlut[hash].len; ++i)
    {
        if (!strcmp(name_en, permons[pmonlut[hash].list[i]].name_en))
        {
            return pmonlut[hash].list[i];
        }
    }
    return NO_PMON;
}

void mon_init(void)
{
    int i;
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        if (!permons[i].valid)
        {
            last_populated_permon = i - 1;
            break;
        }
        pmonlut_register(i);
    }
    return;
}

/* monsters.c */
