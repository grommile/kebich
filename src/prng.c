/*! \file prng.c
 * \brief ChaCha stream cipher-based PRNG
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#ifdef linux
#include <endian.h>
#else 
#error Please add a stanza in rng.c to specify a header file for your platform which defines the endian-conversion functions htobe32, be32toh, etc.
#endif
#include "prng.h"

/*! \brief Implementation of ChaCha quarter round
 *
 * A naïve implementation, cribbed from RFC7539 section 2.1.
 */
static inline void quarter_round(uint32_t *a, uint32_t *b, uint32_t *c, uint32_t *d)
{
    *a += *b; *d ^= *a; *d = ((*d & 0xffff0000u) >> 16) | ((*d & 0x0000ffffu) << 16);
    *c += *d; *b ^= *c; *b = ((*b & 0xfff00000u) >> 20) | ((*b & 0x000fffffu) << 12);
    *a += *b; *d ^= *a; *d = ((*d & 0xff000000u) >> 24) | ((*d & 0x00ffffffu) << 8);
    *c += *d; *b ^= *c; *b = ((*b & 0xfe000000u) >> 25) | ((*b & 0x01ffffffu) << 7);
}

static inline void column_round(uint32_t *buf)
{
    quarter_round(buf + 0, buf + 4, buf +  8, buf + 12);
    quarter_round(buf + 1, buf + 5, buf +  9, buf + 13);
    quarter_round(buf + 2, buf + 6, buf + 10, buf + 14);
    quarter_round(buf + 3, buf + 7, buf + 11, buf + 15);
}

static inline void diagonal_round(uint32_t *buf)
{
    quarter_round(buf + 0, buf + 5, buf + 10, buf + 15);
    quarter_round(buf + 1, buf + 6, buf + 11, buf + 12);
    quarter_round(buf + 2, buf + 7, buf +  8, buf + 13);
    quarter_round(buf + 3, buf + 4, buf +  9, buf + 14);
}

static inline void double_round(uint32_t *buf)
{
    column_round(buf);
    diagonal_round(buf);
}

static void chacha20_run(uint32_t *buf, uint32_t const *key, uint32_t counter, uint32_t const *nonce)
{
    int i;
    buf[0] = CHACHA_CONST0;
    buf[1] = CHACHA_CONST1;
    buf[2] = CHACHA_CONST2;
    buf[3] = CHACHA_CONST3;
    memcpy(buf + 4, key, 32);
    buf[12] = counter;
    memcpy(buf + 13, nonce, 12);
    for (i = 0; i < 10; ++i)
    {
        double_round(buf);
    }
}

size_t cc20_savesize(void)
{
    return 56;
}

/*! \brief Serialize a ChaCha20 PRNG object to a buffer in BE32 format
 *
 * Serialize the contents of the PRNG object into an array of uint32_t in
 * big-endian order (a.k.a. "network byte order").
 */
void ser_cc20_prng(ChaCha20PRNG const *generator, uint32_t *buffer)
{
    int i;
    for (i = 0; i < 8; ++i)
    {
        buffer[i] = htobe32(generator->key[i]);
    }
    for (i = 0; i < 3; ++i)
    {
        buffer[8 + i] = htobe32(generator->nonce[i]);
    }
    buffer[12] = htobe32(generator->block_counter);
    buffer[13] = htobe32(generator->word_counter);
}

/*! \brief Deserialize a ChaCha20 PRNG object from a buffer in BE32 format
 *
 * Deserialize the contents of an array of uint32_t in big-endian order (a.k.a.
 * "network byte order") into a PRNG object.
 */
void deser_cc20_prng(ChaCha20PRNG *generator, uint32_t const *buffer)
{
    int i;
    for (i = 0; i < 8; ++i)
    {
        generator->key[i] = be32toh(buffer[i]);
    }
    for (i = 0; i < 3; ++i)
    {
        generator->nonce[i] = be32toh(buffer[8 + i]);
    }
    generator->block_counter = be32toh(buffer[12]);
    generator->word_counter = be32toh(buffer[13]);
    chacha20_run(generator->buffer, generator->key, generator->block_counter, generator->nonce);
}

void init_cc20_prng(ChaCha20PRNG *generator, uint32_t const *key, uint32_t const *nonce)
{
    assert(generator && key && nonce);
    memcpy(generator->key, key, sizeof generator->key);
    memcpy(generator->nonce, nonce, sizeof generator->nonce);
    generator->block_counter = 0;
    generator->word_counter = 0;
    chacha20_run(generator->buffer, generator->key, generator->block_counter, generator->nonce);
}

uint32_t invoke_cc20_prng(ChaCha20PRNG *generator)
{
    uint32_t outval = generator->buffer[generator->word_counter];
    ++(generator->word_counter);
    if (generator->word_counter >= CHACHA_BLOCK_WORDS)
    {
        generator->word_counter = 0;
        ++(generator->block_counter);
        chacha20_run(generator->buffer, generator->key, generator->block_counter, generator->nonce);
    }
    return outval;
}

#ifdef PRNG_TEST
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

int main(void)
{
    ChaCha20PRNG testcase;
    ChaCha20PRNG second_testcase __attribute__ ((unused));
    uint32_t keybuf[CHACHA_KEY_WORDS];
    uint32_t noncebuf[CHACHA_NONCE_WORDS];
    struct timeval start_tv;
    struct timeval end_tv;
    int fd;
    int i;
    uint32_t k = 0;
    uint32_t l = 0;
    fd = open("/dev/urandom", O_RDONLY, 0);
    if (fd == -1)
    {
        fputs("prng_perf_test: notice: couldn't open /dev/urandom\n", stderr);
        srand(time(NULL));
        for (i = 0; i < CHACHA_KEY_WORDS; ++i)
        {
            keybuf[i] = rand();
        }
        for (i = 0; i < 3; ++i)
        {
            noncebuf[i] = rand();
        }
    }
    else
    {
        ssize_t s;
        s = read(fd, keybuf, sizeof testcase.key);
        if (s != sizeof testcase.key)
        {
            fputs("prng_perf_test: Failed or short read from /dev/urandom\n", stderr);
            exit(1);
        }
        s = read(fd, noncebuf, sizeof testcase.nonce);
        if (s != sizeof testcase.nonce)
        {
            fputs("prng_perf_test: Failed or short read from /dev/urandom\n", stderr);
            exit(1);
        }
    }
    init_cc20_prng(&testcase, keybuf, noncebuf);
    printf("Invoking PRNG one hundred million times\n");
    gettimeofday(&start_tv, NULL);
    for (i = 0; i < 100000000; ++i)
    {
        k = invoke_cc20_prng(&testcase);
        l ^= k;
    }
    gettimeofday(&end_tv, NULL);
    printf("Start time: %d.%06d\n", (int) start_tv.tv_sec, (int) start_tv.tv_usec);
    printf("  End time: %d.%06d\n", (int) end_tv.tv_sec, (int) end_tv.tv_usec);
    printf("last k is %8.8x\n", k);
    printf("l is %8.8x\n", l);
    printf("32 outputs from generator:\n");
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase));
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase));
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase));
    printf("%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase),
           invoke_cc20_prng(&testcase), invoke_cc20_prng(&testcase));
}
#endif

/* prng.c */
