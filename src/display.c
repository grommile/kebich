/*! \file display.c
 * \brief ncurses display routines for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define DISPLAY_C
#include "kebich.h"
#include "objfun.h"
#include "monfun.h"
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <panel.h>
#include <stdarg.h>
#include <string.h>

WINDOW *status_window;
WINDOW *world_window;
WINDOW *message_window;
PANEL *status_panel;
PANEL *world_panel;
PANEL *message_panel;

bool status_updated;
bool map_updated;
bool display_initialized;

enum game_cpairs
{
    Gcp_red_on_black = 1,
    Gcp_green_on_black = 2,
    Gcp_yellow_on_black = 3,
    Gcp_blue_on_black = 4,
    Gcp_magenta_on_black = 5,
    Gcp_cyan_on_black = 6,
    Gcp_white_on_black = 7,
    Gcp_black_on_black = 8,
};

static short const gcol_cpairs[16] = {
    0,
    Gcp_red_on_black,
    Gcp_green_on_black,
    Gcp_yellow_on_black,
    Gcp_blue_on_black,
    Gcp_magenta_on_black,
    Gcp_cyan_on_black,
    Gcp_white_on_black,
    Gcp_black_on_black,
    Gcp_red_on_black,
    Gcp_green_on_black,
    Gcp_yellow_on_black,
    Gcp_blue_on_black,
    Gcp_magenta_on_black,
    Gcp_cyan_on_black,
    Gcp_white_on_black
};

static int const gcol_attrbits[16] = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    A_BOLD,
    A_BOLD,
    A_BOLD,
    A_BOLD,
    A_BOLD,
    A_BOLD,
    A_BOLD,
    A_BOLD
};

static inline int gcol_attr(Game_colour gc) PUREFUNC;
static inline int gcol_attr(Game_colour gc) { return gcol_attrbits[gc] | COLOR_PAIR(gcol_cpairs[gc]); }
/* Prototypes for static funcs */
static int object_char(int object_id);
static int monster_char(int monster_id);
static int terrain_char(Terrain terrain_type) CONSTFUNC;
static int terrain_attr(Terrain terrain_type) PUREFUNC;
static void draw_status_line(void);
static void draw_world(void);

static Game_colour fraction_colour(int32_t num, int32_t den, Game_colour equal)
{
    // Standard thresholds 1/3, (1 - 1/3), 1.
    if (num > den)
    {
        return Gcol_white;
    }
    else if (num > (den - den / 3))
    {
        return equal;
    }
    else if (num > (den / 3))
    {
        return Gcol_yellow;
    }
    else
    {
        return Gcol_red;
    }
}

/* Static funcs */
static void draw_status_line(void)
{
    wattrset(status_window, gcol_attr(Gcol_green));
    mvwprintw(status_window, 0, 0, "%-16.16s", u.name);
    wattrset(status_window, gcol_attr(Gcol_light_green));
    mvwaddstr(status_window, 0, CAMERA_SIZE + 2, "HP: ");
    wattrset(status_window, gcol_attr(fraction_colour(u.hpcur, u.hpmax, Gcol_light_green)));
    mvwprintw(status_window, 0, CAMERA_SIZE + 6, "%03d", u.hpcur);
    wattrset(status_window, gcol_attr(Gcol_light_green));
    mvwprintw(status_window, 0, CAMERA_SIZE + 9, "/%03d", u.hpmax);
    wattrset(status_window, gcol_attr(Gcol_light_blue));
    mvwprintw(status_window, 1, CAMERA_SIZE + 2, "SH: ");
    if (u.shmax == 0)
    {
        mvwaddstr(status_window, 1, CAMERA_SIZE + 6, "---/---");
    }
    else
    {
        wattrset(status_window, gcol_attr(fraction_colour(u.shcur, u.shmax, Gcol_light_blue)));
        mvwprintw(status_window, 1, CAMERA_SIZE + 6, "%03d", u.shcur);
    }
    wattrset(status_window, gcol_attr(Gcol_light_blue));
    mvwprintw(status_window, 1, CAMERA_SIZE + 9, "/%03d", u.shmax);
    wattrset(status_window, gcol_attr(Gcol_green));
    mvwprintw(status_window, 1, 40, "Depth: %d", depth);
}

static int terrain_char(Terrain terrain_type)
{
    switch (terrain_type)
    {
    case STAIRS:
        return '>';
    case FLOOR:
        return '.';
    case WALL:
        return '#';
    case DOOR:
        return '+';
    default:
        return '*';
    }
}

static int terrain_attr(Terrain terrain_type)
{
    switch (terrain_type)
    {
    case DOOR:
    case STAIRS:
        return gcol_attr(Gcol_light_cyan);
    default:
        return gcol_attr(Gcol_green);
    }
}

static int monster_char(int monster_id)
{
    return permons[monster_id].sym;
}

static int object_char(int object_id)
{
    return permobjs[object_id].sym;
}

static void draw_world(void)
{
    int i;
    int j;
    int x;
    int y;
    for (i = -10; i < 11; i++)
    {
        y = u.y + i;
        for (j = -10; j < 11; j++)
        {
            x = u.x + j;
            if ((i == 0) && (j == 0))
            {
                wattrset(world_window, gcol_attr(Gcol_light_blue));
                mvwaddch(world_window, 10, 10, '@');
            }
            else if ((y < 0) || (x < 0) ||
                     (y >= DUN_SIZE) || (x >= DUN_SIZE))
            {
                mvwaddch(world_window, i + 10, j + 10, ' ');
            }
            else if ((mapmonster[y][x] != NO_MON) && tile_in_sight(y, x))
            {
                wattrset(world_window, gcol_attr(Gcol_red));
                mvwaddch(world_window, i + 10, j + 10, monster_char(monsters[mapmonster[y][x]].mon_id));
            }
            else if (mapflags[y][x] & MAPFLAG_EXPLORED)
            {
                if (mapobject[y][x] != NO_OBJ)
                {
                    wattrset(world_window, gcol_attr(Gcol_yellow));
                    mvwaddch(world_window, i + 10, j + 10, object_char(objects[mapobject[y][x]].obj_id));
                }
                else
                {
                    wattrset(world_window, terrain_attr(terrain[y][x]));
                    mvwaddch(world_window, i + 10, j + 10, terrain_char(terrain[y][x]));
                }
            }
            else
            {
                mvwaddch(world_window, i + 10, j + 10, ' ');
            }
        }
    }
}

/* extern funcs */
void display_update(void)
{
    if (status_updated)
    {
        status_updated = 0;
        draw_status_line();
    }
    if (map_updated)
    {
        map_updated = 0;
        draw_world();
    }
    update_panels();
    doupdate();
}

int display_init(void)
{
    int i;
    initscr();
    if (!has_colors())
    {
        endwin();
        fputs("kebich: Monochrome terminals not supported.\n", stderr);
        exit(1);
    }
    noecho();
    cbreak();
    start_color();
    init_pair(Gcp_red_on_black, COLOR_RED, COLOR_BLACK);
    init_pair(Gcp_green_on_black, COLOR_GREEN, COLOR_BLACK);
    init_pair(Gcp_yellow_on_black, COLOR_YELLOW, COLOR_BLACK);
    init_pair(Gcp_blue_on_black, COLOR_BLUE, COLOR_BLACK);
    init_pair(Gcp_magenta_on_black, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(Gcp_cyan_on_black, COLOR_CYAN, COLOR_BLACK);
    init_pair(Gcp_white_on_black, COLOR_WHITE, COLOR_BLACK);
    init_pair(Gcp_black_on_black, COLOR_BLACK, COLOR_BLACK);
    /* OK. We want a 22x22 viewport (player at centre, and a one-wide frame), a
     * 21x58 message window, and a 2x80 status line. */
    status_window = newwin(2, 80, 22, 0);
    status_panel = new_panel(status_window);
    world_window = newwin(CAMERA_SIZE + 1, CAMERA_SIZE + 1, 0, 0);
    world_panel = new_panel(world_window);
    message_window = newwin(CAMERA_SIZE, 80 - CAMERA_SIZE, 0, CAMERA_SIZE + 1);
    message_panel = new_panel(message_window);
    wclear(status_window);
    wclear(world_window);
    wclear(message_window);
    scrollok(status_window, FALSE);
    scrollok(world_window, FALSE);
    scrollok(message_window, TRUE);
    wattrset(status_window, gcol_attr(Gcol_green));
    wattrset(message_window, gcol_attr(Gcol_green));
    wattrset(world_window, gcol_attr(Gcol_green));
    for (i = 0; i < CAMERA_SIZE; ++i)
    {
        mvwaddch(world_window, CAMERA_SIZE, i, '-');
        mvwaddch(world_window, i, CAMERA_SIZE, '|');
    }
    mvwaddch(world_window, CAMERA_SIZE, CAMERA_SIZE, '+');
    wattrset(world_window, gcol_attr(Gcol_light_green));
    mvwprintw(world_window, 9, 7, "KEBICH");
    mvwprintw(world_window, 10, 7, "======");
    wattrset(world_window, gcol_attr(Gcol_green));
    wmove(message_window, 0, 0);
    display_initialized = true;
    map_updated = false;
    status_updated = false;
    update_panels();
    doupdate();
    return 0;
}

int read_input(char *buffer, int length)
{
    echo();
    display_update();
    buffer[0] = '\0';
    wgetnstr(message_window, buffer, length);
    noecho();
    return strlen(buffer);
}

void notify_ukillm(int mon)
{
    print_msg("You kill ");
    print_mon_name(mon, 1);
    print_msg("!\n");
}

/*! \brief report a monster killing another monster
 *
 * \todo make this mention the killer as well as the victim 
 */
void notify_mkillm(int er __attribute__((unused)), int ee)
{
    print_mon_name(ee, 2);
    print_msg(" dies.\n");
}

/*! \brief report a monster dying of unknown causes */
void notify_mondie(int mon)
{
    print_mon_name(mon, 2);
    print_msg(" dies.\n");
}

/*! \brief reports that the player's shield has been broken */
void notify_shield_broken(void)
{
    wattrset(message_window, gcol_attr(Gcol_light_blue));
    print_msg("Your shield has been broken!\n");
    wattrset(message_window, gcol_attr(Gcol_green));
}

/*! \brief reports that the player consumed their last item of a certain type
 *
 * \param po the index of the permobj corresponding to the item used up
 */
void notify_used_last(int po)
{
    print_msg("You have no more %s.\n", permobjs[po].plural_en);
}

/*! \brief report the player readying a ranged weapon */
void notify_ready_ranged(int obj)
{
    print_msg("Readying ");
    print_obj_name(obj);
    print_msg(".\n");
}

/*! \brief report the player wielding a weapon */
void notify_wield(int obj)
{
    print_msg("Wielding ");
    print_obj_name(obj);
    print_msg(".\n");
}

/*! \brief report the player setting aside their weapon */
void notify_unwield(int obj)
{
    print_msg("Setting aside ");
    print_obj_name(obj);
    print_msg(".\n");
}

/*! \brief report the player wearing a piece of armour */
void notify_wear(int obj)
{
    print_msg("Wearing ");
    print_obj_name(obj);
    print_msg(".\n");
}

void notify_take_off(int obj)
{
    print_msg("Taking off ");
    print_obj_name(obj);
    print_msg(".\n");
}

/*! \brief report the player plugging in a plugin */
void notify_plug_in(int obj)
{
    print_msg("Plugging in ");
    print_obj_name(obj);
    print_msg(".\n");
}

void notify_remove(int obj)
{
    print_msg("You remove ");
    print_obj_name(obj);
    print_msg(".\n");
}

void notify_use_useless(void)
{
    print_msg("That item doesn't appear to be usable that way.\n");
}

void notify_get(int obj)
{
    print_msg("You get ");
    print_obj_name(obj);
    print_msg(".\n");
}

void notify_now_have(int slot)
{
    print_msg("You now have\n");
    print_msg("%c) ", 'a' + slot);
    print_obj_name(u.inventory[slot]);
    print_msg("\n");
}

void notify_death(Death d, char const *what)
{
    print_msg("You have died.\n");
    switch (d)
    {
    case DEATH_KILLED:
        print_msg("You were killed by %s.\n", what);
        break;
    case DEATH_KILLED_MON:
        print_msg("You were killed by a nasty %s.\n", what);
        break;
    }
    print_msg("Your game lasted %d ticks.\n", game_tick);
    print_msg("You survived until map %d.\n", depth);
    print_msg("You looted $%d.\n", u.moniez);
}

/*! \brief Print a message
 *
 * Print a message to the message window. If the display subsystem has not
 * been initialized yet, print to stdout instead.
 *
 * \todo Implement message flood protection
 * \todo Compose the message string, chop it into words, and linebreak it properly.
 * \param fmt printf argument string */
void print_msg(const char *fmt, ...)
{
    va_list ap;
    if (display_initialized)
    {
        va_start(ap, fmt);
        vw_printw(message_window, fmt, ap);
        va_end(ap);
        display_update();
    }
    else
    {
        vprintf(fmt, ap);
    }
}

void print_inv(Inv_filter filter, intptr_t filter_param)
{
    int i;
    for (i = 0; i < 19; i++)
    {
        if ((u.inventory[i] != NO_OBJ) && ((!filter) || (filter(u.inventory[i], filter_param))))
        {
            print_msg("%c) ", 'a' + i);
            print_obj_name(u.inventory[i]);
            if (u.plugin == u.inventory[i])
            {
                print_msg(" (plugin module)");
            }
            else if (u.melee_weap == u.inventory[i])
            {
                print_msg(" (melee weapon)");
            }
            else if (u.ranged_weap == u.inventory[i])
            {
                print_msg(" (ranged weapon)");
            }
            else if (u.armour == u.inventory[i])
            {
                print_msg(" (being worn)");
            }
            print_msg("\n");
        }
    }
}

int inv_select(char const *action, Inv_filter filter, intptr_t filter_param)
{
    int selection = NO_SLOT;
    int obj;
    int ch;
    print_msg("Items available to %s\n", action);
    print_inv(filter, filter_param);
tryagain:
    print_msg("What do you want to %s?\n", action);
    ch = getch();
    switch (ch)
    {
    case 'x':
    case '\x1b':
    case ' ':
        print_msg("Never mind.\n");
        return NO_SLOT;
    case 'a': case 'b': case 'c': case 'd':
    case 'e': case 'f': case 'g': case 'h':
    case 'i': case 'j': case 'k': case 'l':
    case 'm': case 'n': case 'o': case 'p':
    case 'q': case 'r': case 's':
        selection = ch - 'a';
        obj = u.inventory[selection];
        if (obj == NO_OBJ)
        {
            print_msg("No item in that slot.\n");
            goto tryagain;
        }
        if ((!filter) || (filter(obj, filter_param)))
        {
            return selection;
        }
        else
        {
            print_msg("Unsuitable item.\n");
            goto tryagain;
        }
    default:
        print_msg("No such slot.\n");
        goto tryagain;
    }
    return selection;
}

Passfail select_dir(int *psy, int *psx)
{
    int ch;
    int done = 0;
    print_msg("Select a direction [ESC or space to cancel].\n");
    while (!done)
    {
        ch = getch();
        switch (ch)
        {
        case 'h':
            *psx = -1;
            *psy = 0;
            done = 1;
            break;
        case 'j':
            *psx = 0;
            *psy = 1;
            done = 1;
            break;
        case 'k':
            *psx = 0;
            *psy = -1;
            done = 1;
            break;
        case 'l':
            *psx = 1;
            *psy = 0;
            done = 1;
            break;
        case 'y':
            *psx = -1;
            *psy = -1;
            done = 1;
            break;
        case 'u':
            *psx = 1;
            *psy = -1;
            done = 1;
            break;
        case 'b':
            *psx = -1;
            *psy = 1;
            done = 1;
            break;
        case 'n':
            *psx = 1;
            *psy = 1;
            done = 1;
            break;
        case '\x1b':
        case ' ':
            return PF_fail;  /* cancelled. */
        default:
            print_msg("Bad direction (choices are hjklyubn).\n");
            print_msg("[Press ESC or space to cancel.]\n");
            break;
        }
    }
    return PF_pass;
}

bool get_command(Full_cmd *dest)
{
    int ch;
    int done = 0;
    int i;
    while (!done)
    {
        ch = getch();
        switch (ch)
        {
        case 'a':
            {
                Direction *s = malloc(sizeof (Direction));
                Passfail pf = select_dir(&(s->y), &(s->x));
                if (pf == PF_pass)
                {
                    dest->cmd = ATTACK;
                    dest->extra = (intptr_t) s;
                    return true;
                }
                else
                {
                    free(s);
                }
            }
            break;
        case ',':
        case 'g':
            dest->cmd = GET_ITEM;
            return true;
        case 'd':
            {
                i = inv_select("drop", NULL, 0);
                if (i != NO_SLOT)
                {
                    int obj = u.inventory[i];
                    if (obj == NO_OBJ)
                    {
                        // shouldn't happen! 
                        break;
                    }
                    else if (obj == u.plugin)
                    {
                        print_msg("You can't drop that right now. 'R'emove it first.\n");
                        break;
                    }
                    if (obj == u.armour)
                    {
                        print_msg("You can't drop that right now. 'T'ake it off first.\n");
                        break;
                    }
                    dest->cmd = DROP_ITEM;
                    dest->extra = i;
                    return true;
                }
            }
            break;
        case 'S':
            dest->cmd = SAVE_GAME;
            return true;
        case 'X':
            {
                i = getyn("Really quit?\n");
                if (i > 0)
                {
                    dest->cmd = QUIT;
                    return true;
                }
            }
            break;
        case 'i':
            print_msg("You are carrying:\n");
            print_inv(NULL, 0);
            break;
        case 'I':
            {
                i = inv_select("inspect", NULL, 0);
                if (i != -1)
                {
                    print_msg("%s\n", permobjs[objects[u.inventory[i]].obj_id].description_en);

                }
            }
            break;
        case 'h':
            dest->cmd = MOVE_WEST;
            return true;
        case 'j':
            dest->cmd = MOVE_SOUTH;
            return true;
        case 'k':
            dest->cmd = MOVE_NORTH;
            return true;
        case 'l':
            dest->cmd = MOVE_EAST;
            return true;
        case 'y':
            dest->cmd = MOVE_NW;
            return true;
        case 'u':
            dest->cmd = MOVE_NE;
            return true;
        case 'b':
            dest->cmd = MOVE_SW;
            return true;
        case 'n':
            dest->cmd = MOVE_SE;
            return true;
        case 'z':
            {
                i = inv_select("use", classic_inv_filter, (intptr_t) POCLASS_CONSUMABLE);
                if (i >= 0)
                {
                    dest->cmd = USE_CONSUMABLE;
                    dest->extra = i;
                    return true;
                }
            }
            break;
        case 'w':
            {
                i = inv_select("wield", classic_inv_filter, (intptr_t) POCLASS_WEAPON);
                if (i >= 0)
                {
                    dest->cmd = WIELD_WEAPON;
                    dest->extra = u.inventory[i];
                    return true;
                }
            }
            break;
        case 'W':
            {
                if (u.armour != NO_OBJ)
                {
                    print_msg("You are already wearing some armour. 'T'ake it off first.\n");
                }
                else
                {
                    i = inv_select("wear", classic_inv_filter, (intptr_t) POCLASS_ARMOUR);
                    if (i >= 0)
                    {
                        dest->cmd = WEAR_ARMOUR;
                        dest->extra = u.inventory[i];
                        return true;
                    }
                }
            }
            break;
        case 'T':
            if (u.armour != NO_OBJ)
            {
                dest->cmd = TAKE_OFF_ARMOUR;
                return true;
            }
            else
            {
                print_msg("You aren't wearing any armour.\n");
            }
            break;
        case 'P':
            {
                if (u.plugin != NO_OBJ)
                {
                    print_msg("You already have a plugin module fitted. 'R'emove it first.\n");
                }
                i = inv_select("plug in", classic_inv_filter, (intptr_t) POCLASS_PLUGIN);
                if (i >= 0)
                {
                    dest->cmd = PLUG_IN_PLUGIN;
                    dest->extra = u.inventory[i];
                    return true;
                }
            }
            break;
        case 'R':
            if (u.plugin != NO_OBJ)
            {
                dest->cmd = REMOVE_PLUGIN;
                return true;
            }
            else
            {
                print_msg("You don't have a plugin module plugged in.\n");
            }
            break;
        case '?':
            print_help();
            break;
        case '>':
            dest->cmd = GO_DOWN_STAIRS;
            return true;
        case '.':
            dest->cmd = STAND_STILL;
            return true;
        }
    }
    return 0;
}

int display_shutdown(bool skip_prompt)
{
    if (display_initialized)
    {
        display_update();
        if (!skip_prompt)
        {
            print_msg("Press RETURN/ENTER to exit.\n");
            do
            {
            } while (getch() != '\n');
        }
        clear();
        refresh();
        endwin();
    }
    display_initialized = false;
    return 0;
}

int getyn(const char *msg)
{
    int ch;
    print_msg("%s", msg);
    while (1)
    {
        ch = getch();
        switch (ch)
        {
        case 'y':
        case 'Y':
            return 1;
        case 'n':
        case 'N':
            return 0;
        case '\x1b':
        case ' ':
            return -1;
        default:
            print_msg("Invalid response. Press y or n (ESC or space to cancel)\n");
        }
    }
}

void print_help(void)
{
    print_msg("MOVEMENT\n");
    print_msg("y  k  u\n");
    print_msg(" \\ | /\n");
    print_msg("  \\|/ \n");
    print_msg("h--*--l\n");
    print_msg("  /|\\ \n");
    print_msg(" / | \\\n");
    print_msg("b  j  n\n");
    print_msg("Attack monsters in melee by bumping into them.\n");
    print_msg("Doors do not have to be opened before you go through.\n");
    print_msg("\nPress any key to continue...\n");
    getch();
    print_msg("ACTIONS\n");
    print_msg("a   make an attack (used to fire bows)\n");
    print_msg("P   plug in a plugin module\n");
    print_msg("R   remove a plugin module\n");
    print_msg("W   wear armour\n");
    print_msg("T   take off armour\n");
    print_msg("z   use a consumable\n");
    print_msg("w   wield a weapon\n");
    print_msg("g   pick up an item\n");
    print_msg("d   drop an item\n");
    print_msg(">   go down stairs\n");
    print_msg(".   do nothing (wait until next action)\n");
    print_msg("\nPress any key to continue...\n");
    getch();
    print_msg("OTHER COMMANDS\n");
    print_msg("S   save and exit\n");
    print_msg("X   quit without saving\n");
    print_msg("i   print your inventory\n");
    print_msg("?   print this message\n");
    print_msg("\nPress any key to continue...\n");
    getch();
    print_msg("SYMBOLS\n");
    print_msg("@   you\n");
    print_msg(".   floor\n");
    print_msg(">   stairs down\n");
    print_msg("#   wall\n");
    print_msg("+   a door\n");
    print_msg(")   a melee weapon\n");
    print_msg("(   a missile weapon\n");
    print_msg("[   a suit of armour\n");
    print_msg("=   a plugin module\n");
    print_msg("!   a consumable item\n");
    print_msg("\nRed symbols are enemies. Yellow symbols are items\n");
    print_msg("you can pick up.\n");
    print_msg("\nThis is all the help you get. Good luck!\n");
}

/*! \brief Get the player character's name. */
int get_player_name(char *buf, int max_printables)
{
    int i;
    print_msg("What is your name, stranger?\n");
    i = read_input(u.name, max_printables);
    if (i == 0)
    {
        print_msg("I didn't hear you.\n");
        print_msg("I'll call you Melisande.\n");
        buf[max_printables] = '\0';
        strncpy(buf, "Melisande", max_printables);
    }
    return i;
}

/* display.c */
