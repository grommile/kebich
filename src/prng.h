/*! \file prng.h
 * \brief Header file for ChaCha stream cipher-based PRNG
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_prng_h
#define INC_prng_h

#include <stddef.h>
#include <stdint.h>

/*! \brief constant word 0 specified by RFC 7539 section 2.3 */
#define CHACHA_CONST0 0x61707865
/*! \brief constant word 1 specified by RFC 7539 section 2.3 */
#define CHACHA_CONST1 0x3320646e
/*! \brief constant word 2 specified by RFC 7539 section 2.3 */
#define CHACHA_CONST2 0x79622d32
/*! \brief constant word 3 specified by RFC 7539 section 2.3 */
#define CHACHA_CONST3 0x6b206574

#define CHACHA_KEY_WORDS 8
#define CHACHA_NONCE_WORDS 3
#define CHACHA_BLOCK_WORDS 16
#define CHACHA_WORDCOUNT_MASK 0xfu
#define CHACHA_DBL_ROUNDS 10
#define CHACHA_RAND_MAX 0xffffffffu
typedef struct cc20_prng
{
    // output buffer of algorithm
    uint32_t buffer[CHACHA_BLOCK_WORDS];
    // variable inputs to algorithm
    uint32_t key[CHACHA_KEY_WORDS];
    uint32_t block_counter;
    uint32_t nonce[CHACHA_NONCE_WORDS];
    // PRNG state
    uint32_t word_counter; // which word should we issue?
} ChaCha20PRNG;

extern size_t cc20_savesize(void) __attribute__((const));
extern void ser_cc20_prng(ChaCha20PRNG const *generator, uint32_t *buffer);
extern void deser_cc20_prng(ChaCha20PRNG *generator, uint32_t const *buffer);
extern void init_cc20_prng(ChaCha20PRNG *generator, uint32_t const *key, uint32_t const *nonce);
extern uint32_t invoke_cc20_prng(ChaCha20PRNG *generator);

#endif

/* prng.h */
