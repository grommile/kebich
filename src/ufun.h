/*! \file ufun.h
 * \brief player functions header file for Kebich
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once
#ifndef INC_ufun_h
#define INC_ufun_h
#include "kebich.h"

extern void u_init(void);
extern void do_death(enum death d, const char *what);
extern void heal_u(int amount, int boost);
extern int damage_u(int amount, bool bypass_shield, enum death d, const char *what);
extern int lev_threshold(int level) CONSTFUNC;
extern int move_player(int dy, int dx);
extern int reloc_player(int y, int x);
extern void recalc_derived(void);
extern void teleport_u(void);
extern int get_free_inventory_slot(void);
extern void bookkeep_u(void);
extern void restore_shield(int16_t amount);
extern void boost_shield(int16_t amount);
extern void gain_level(void);

#endif

/* ufun.h */
