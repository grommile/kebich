/*! \file permons.c
 * \brief "permon" handling for Kebich
 *
 * Permons contain the prototypical properties for monsters.
 */

#define PERMONS_C
#include "kebich.h"

int last_populated_permon = MAX_PERMON_SLOTS - 1;

char const *faction_names_en[] = {
    "neutral", "friendly", "hostile", "betrayed"
};

/*! \brief the monster database */
Permon const permons[MAX_PERMON_SLOTS] = {
    {
        true, "child", "children", 'h', 80, 0,
        8, 0, 1, 0,
        Speed_normal, AI_escaper,
        Faction_neutral,
        { 0, 0 }
    },
    {
        true, "adult", "adults", 'H', 80, 0,
        16, 0, 4, 0,
        Speed_normal, AI_escaper,
        Faction_neutral,
        { 0, 0 }
    },
    {
        true, "rat", "rats", 'r', 15, 1,
        4, 0, 2, 4,
        Speed_fast, AI_escaper,
        Faction_hostile,
        { 0, 0 }
    },
    {
        true, "big rat", "big rats", 'R', 15, 2,
        8, 0, 2, 1,
        Speed_fast, AI_charger,
        Faction_hostile,
        { 0, 0 }
    },
    {
        true, "glitch", "glitches", 'g', 20, 4,
        20, 0, 4, 0,
        Speed_normal, AI_charger,
        Faction_hostile,
        { 0, 0 }
    },
    {
        true, "doberbot", "doberbots", 'd', 25, 5,
        25, 0, 6, 0,
        Speed_fast, AI_charger,
        Faction_hostile,
        { 0, 0 }
    },
    {
        true, "snek", "sneks", 's', 25, 6,
        30, 0, 8, 0,
        Speed_fast, AI_charger,
        Faction_hostile,
        { 0, 0 }
    },
    {
        true, "processor node", "processor nodes", 'P', 100, 1,
        100, 0, 0, 0,
        Speed_slow, AI_supine,
        Faction_hostile,
        { 0, 0 }
    },
};

Permon_stat permon_stats[MAX_PERMON_SLOTS];

/* permons.c */
