/*! \file main.c
 * \brief main() and friends for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include "kebich.h"
#include "kebich-io.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "mapfun.h"
#include "ufun.h"
#include "prng.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
#include <sys/time.h>
#endif
#include <time.h>

void save_game(void);
static void ser_map(FILE *fp);
static void ser_Player(FILE *fp, Player const *pptr);
static void ser_Mon(FILE *fp, Mon const *mptr);
static void ser_Obj(FILE *fp, Obj const *optr);
static void ser_Permon_stat(FILE *fp, Permon_stat const *pmsptr);
static void ser_Permobj_stat(FILE *fp, Permobj_stat const *posptr);
static void deser_map(FILE *fp);
static void deser_Player(FILE *fp, Player *pptr);
static void deser_Mon(FILE *fp, Mon *mptr);
static void deser_Obj(FILE *fp, Obj *optr);
static void deser_Permon_stat(FILE *fp, Permon_stat *pmsptr);
static void deser_Permobj_stat(FILE *fp, Permobj_stat *posptr);
static void load_game(void);
static void new_game(void);
static void rebuild_mapobjs(void);
static void rebuild_mapmons(void);
static void rnum_init(bool use_strong);
static void main_loop(void);
static bool do_command(Full_cmd *fcmd);
static ChaCha20PRNG running_generator;
static ChaCha20PRNG saved_generator;
bool game_finished = false;
int32_t game_tick = 0;

#define WORKBUF_DECL(size) uint8_t work_buffer[(size)]; uint8_t *wbuf_ptr MYUNUSED = work_buffer
#define WORKBUF_PUSH(x, size) do { memcpy(wbuf_ptr, (x), (size)); wbuf_ptr += (size); } while (0)
#define WORKBUF_PUSH_8(x) do { *(wbuf_ptr++) = x; } while (0)
#define WORKBUF_PUSH_16(x) do { tmpi16 = htobe16((x)); WORKBUF_PUSH(&tmpi16, sizeof tmpi16); } while (0)
#define WORKBUF_PUSH_32(x) do { tmpi32 = htobe32((x)); WORKBUF_PUSH(&tmpi32, sizeof tmpi32); } while (0)
#define WORKBUF_PUSH_16ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PUSH_16((x)[i]); } } while (0)
#define WORKBUF_PUSH_32ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PUSH_32((x)[i]); } } while (0)
#define WORKBUF_PULL(x, size) do { memcpy((x), wbuf_ptr, (size)); wbuf_ptr += (size); } while (0)
#define WORKBUF_PULL_8(x) do { *(x) = *(wbuf_ptr++); } while (0)
#define WORKBUF_PULL_16(x) do { WORKBUF_PULL(&tmpi16, sizeof tmpi16); *(x) = tmpi16; } while (0)
#define WORKBUF_PULL_32(x) do { WORKBUF_PULL(&tmpi32, sizeof tmpi32); *(x) = tmpi32; } while (0)
#define WORKBUF_PULL_16ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PULL_16((x) + i); } } while (0)
#define WORKBUF_PULL_32ARR(x, n) do { int i; for (i = 0; i < (n); ++i) { WORKBUF_PULL_32((x) + i); } } while (0)
#define WORKBUF_RESET() do { wbuf_ptr = work_buffer; } while (0)
#define WORKBUF_CLEAR() do { memset(work_buffer, '\0', sizeof work_buffer); } while (0)
#define TMPINTS() uint16_t tmpi16 MYUNUSED; uint32_t tmpi32 MYUNUSED
#define WRITE_WORKBUF(size) do { checked_fwrite(work_buffer, 1, (size), fp); } while (0)
#define READ_WORKBUF(size) do { checked_fwrite(work_buffer, 1, (size), fp); } while (0)

static void ser_map(FILE *fp)
{
    WORKBUF_DECL((sizeof mapflags) + (sizeof terrain) + (sizeof roombounds));
    TMPINTS();
    int y;
    int x;
    int rn;
    for (y = 0; y < DUN_SIZE; ++y)
    {
        for (x = 0; x < DUN_SIZE; ++x)
        {
            WORKBUF_PUSH_32(mapflags[y][x]);
            WORKBUF_PUSH_8(terrain[y][x]);
        }
    }
    for (rn = 0; rn < MAX_ROOMS; ++rn)
    {
        WORKBUF_PUSH_32(roombounds[rn].top_y);
        WORKBUF_PUSH_32(roombounds[rn].bot_y);
        WORKBUF_PUSH_32(roombounds[rn].left_x);
        WORKBUF_PUSH_32(roombounds[rn].right_x);
    }
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_map(FILE *fp)
{
    WORKBUF_DECL((sizeof mapflags) + (sizeof terrain) + (sizeof roombounds));
    TMPINTS();
    int y;
    int x;
    int rn;
    READ_WORKBUF(sizeof work_buffer);
    for (y = 0; y < DUN_SIZE; ++y)
    {
        for (x = 0; x < DUN_SIZE; ++x)
        {
            WORKBUF_PULL_32(mapflags[y] + x);
            WORKBUF_PULL_8(terrain[y] + x);
        }
    }
    for (rn = 0; rn < MAX_ROOMS; ++rn)
    {
        WORKBUF_PULL_32(&(roombounds[rn].top_y));
        WORKBUF_PULL_32(&(roombounds[rn].bot_y));
        WORKBUF_PULL_32(&(roombounds[rn].left_x));
        WORKBUF_PULL_32(&(roombounds[rn].right_x));
    }
}

static void ser_Mon(FILE *fp, Mon const *mptr)
{
    WORKBUF_DECL(sizeof (Mon));
    TMPINTS();
    WORKBUF_PUSH_32(mptr->mon_id);
    WORKBUF_PUSH_32(mptr->y);
    WORKBUF_PUSH_32(mptr->x);
    WORKBUF_PUSH_16(mptr->hpmax);
    WORKBUF_PUSH_16(mptr->hpcur);
    WORKBUF_PUSH_16(mptr->tohit);
    WORKBUF_PUSH_16(mptr->evasion);
    WORKBUF_PUSH_16(mptr->dam);
    WORKBUF_PUSH_16(mptr->current_faction);
    WORKBUF_PUSH_8(mptr->used);
    WORKBUF_PUSH_8(mptr->awake);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Mon(FILE *fp, Mon *mptr)
{
    WORKBUF_DECL(sizeof (Mon));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(mptr->mon_id));
    WORKBUF_PULL_32(&(mptr->y));
    WORKBUF_PULL_32(&(mptr->x));
    WORKBUF_PULL_16(&(mptr->hpmax));
    WORKBUF_PULL_16(&(mptr->hpcur));
    WORKBUF_PULL_16(&(mptr->tohit));
    WORKBUF_PULL_16(&(mptr->evasion));
    WORKBUF_PULL_16(&(mptr->dam));
    WORKBUF_PULL_16(&(mptr->current_faction));
    WORKBUF_PULL_8(&(mptr->used));
    WORKBUF_PULL_8(&(mptr->awake));
}

static void ser_Obj(FILE *fp, Obj const *optr)
{
    WORKBUF_DECL(sizeof (Obj));
    TMPINTS();
    WORKBUF_PUSH_32(optr->obj_id);
    WORKBUF_PUSH_32(optr->quan);
    WORKBUF_PUSH_32(optr->y);
    WORKBUF_PUSH_32(optr->x);
    WORKBUF_PUSH_8(optr->used);
    WORKBUF_PUSH_8(optr->with_you);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Obj(FILE *fp, Obj *optr)
{
    WORKBUF_DECL(sizeof (Obj));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(optr->obj_id));
    WORKBUF_PULL_32(&(optr->quan));
    WORKBUF_PULL_32(&(optr->y));
    WORKBUF_PULL_32(&(optr->x));
    WORKBUF_PULL_8(&(optr->used));
    WORKBUF_PULL_8(&(optr->with_you));
}

static void ser_Permon_stat(FILE *fp, Permon_stat const *pmsptr)
{
    WORKBUF_DECL(sizeof (Permon_stat));
    TMPINTS();
    WORKBUF_PUSH_32(pmsptr->num_generated);
    WORKBUF_PUSH_32(pmsptr->killed_by_player);
    WORKBUF_PUSH_32(pmsptr->killed_by_other);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Permon_stat(FILE *fp, Permon_stat *pmsptr)
{
    WORKBUF_DECL(sizeof (Permon_stat));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(pmsptr->num_generated));
    WORKBUF_PULL_32(&(pmsptr->killed_by_player));
    WORKBUF_PULL_32(&(pmsptr->killed_by_other));
}

static void ser_Permobj_stat(FILE *fp, Permobj_stat const *posptr)
{
    WORKBUF_DECL(sizeof (Permobj_stat));
    TMPINTS();
    WORKBUF_PUSH_32(posptr->num_generated);
    WORKBUF_PUSH_16(posptr->flavour);
    WORKBUF_PUSH_8(posptr->identified);
    WRITE_WORKBUF(sizeof work_buffer);
}

static void deser_Permobj_stat(FILE *fp, Permobj_stat *posptr)
{
    WORKBUF_DECL(sizeof (Permobj_stat));
    TMPINTS();
    READ_WORKBUF(sizeof work_buffer);
    WORKBUF_PULL_32(&(posptr->num_generated));
    WORKBUF_PULL_16(&(posptr->flavour));
    WORKBUF_PULL_8(&(posptr->identified));
}

static void ser_Player(FILE *fp, Player const *pptr)
{
    /* max hp, max shield, tohit, evasion, and shield_regen do not need
     * to be serialized, as they can be reconstructed by calling
     * recalc_derived(). It can thus be safely assumed that the work
     * buffer need not be any larger than the size of a Player. */
    WORKBUF_DECL(sizeof (Player));
    TMPINTS();
    WORKBUF_CLEAR();
    WORKBUF_PUSH(pptr->name, PLAYER_NAME_DISPLAYED);
    WORKBUF_PUSH_32(pptr->y);
    WORKBUF_PUSH_32(pptr->x);
    WORKBUF_PUSH_32(pptr->moniez);
    WORKBUF_PUSH_32ARR(pptr->inventory, INVENTORY_SIZE);
    WORKBUF_PUSH_32(pptr->melee_weap);
    WORKBUF_PUSH_32(pptr->ranged_weap);
    WORKBUF_PUSH_32(pptr->armour);
    WORKBUF_PUSH_32(pptr->plugin);
    WORKBUF_PUSH_16(pptr->hpcur);
    WORKBUF_PUSH_16(pptr->shcur);
    WORKBUF_PUSH_8(pptr->level);
    WRITE_WORKBUF(sizeof *pptr);
}

static void deser_Player(FILE *fp, Player *pptr)
{
    WORKBUF_DECL(sizeof (Player));
    TMPINTS();
    READ_WORKBUF(sizeof (Player));
    WORKBUF_PULL(pptr->name, PLAYER_NAME_DISPLAYED);
    pptr->name[PLAYER_NAME_DISPLAYED] = '\0';
    WORKBUF_PULL_32(&(pptr->y));
    WORKBUF_PULL_32(&(pptr->x));
    WORKBUF_PULL_32(&(pptr->moniez));
    WORKBUF_PULL_32ARR(pptr->inventory, INVENTORY_SIZE);
    WORKBUF_PULL_32(&(pptr->melee_weap));
    WORKBUF_PULL_32(&(pptr->ranged_weap));
    WORKBUF_PULL_32(&(pptr->armour));
    WORKBUF_PULL_32(&(pptr->plugin));
    WORKBUF_PULL_32(&(pptr->hpcur));
    WORKBUF_PULL_32(&(pptr->shcur));
    WORKBUF_PULL_8(&(pptr->level));
}

static void rebuild_roomnums(void)
{
    int i;
    int32_t y;
    int32_t x;
    for (i = 0; i < MAX_ROOMS; ++i)
    {
        for (y = roombounds[i].top_y; y <= roombounds[i].bot_y; ++y)
        {
            for (x = roombounds[i].left_x; x <= roombounds[i].right_x; ++x)
            {
                roomnums[y][x] = i;
            }
        }
    }
}

static void rebuild_mapmons(void)
{
    int i;
    for (i = 0; i < MAX_MONS_IN_PLAY; i++)
    {
        if (monsters[i].used)
        {
            mapmonster[monsters[i].y][monsters[i].x] = i;
        }
    }
}

static void rebuild_mapobjs(void)
{
    int i;
    for (i = 0; i < MAX_OBJS_IN_PLAY; i++)
    {
        if (objects[i].used && !objects[i].with_you)
        {
            mapobject[objects[i].y][objects[i].x] = i;
        }
    }
}

/*! \brief Checked wrapper for fopen
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
FILE *checked_fopen(char const *path, char const *mode)
{
    FILE *fp = fopen(path, mode);
    if (fp == NULL)
    {
        fatal("couldn't open %s with mode %s: %s", path, mode, strerror(errno));
    }
    return fp;
}

/*! \brief Checked wrapper for fwrite
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
void checked_fwrite(void const *buf, size_t size, size_t nmemb, FILE *stream)
{
    size_t s = fwrite(buf, size, nmemb, stream);
    if (s != nmemb)
    {
        fatal("checked_fwrite detected short write");
    }
}

/*! \brief Checked wrapper for fread
 *
 * Only for use in cases where "exit immediately" is an acceptable response.
 */
void checked_fread(void *buf, size_t size, size_t nmemb, FILE *stream)
{
    size_t s = fread(buf, size, nmemb, stream);
    if (s != nmemb)
    {
        fatal("checked_fwrite detected short write");
    }
}

void save_game(void)
{
    FILE *fp;
    int i;
    uint32_t *rngbuf = malloc(cc20_savesize());
    int32_t tmpi32;
    fp = checked_fopen("kebich.sav", "wb");
    if (fp == NULL)
    {
        fatal("couldn't open save file for write: %s", strerror(errno));
    }
    /* Write out the snapshot we took of the RNG before generating the
     * current level. */
    ser_cc20_prng(&saved_generator, rngbuf);
    checked_fwrite(rngbuf, 1, cc20_savesize(), fp);
    ser_map(fp);
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        ser_Permon_stat(fp, permon_stats + i);
    }
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        ser_Permobj_stat(fp, permobj_stats + i);
    }
    for (i = 0; i < MAX_MONS_IN_PLAY; ++i)
    {
        ser_Mon(fp, monsters + i);
    }
    for (i = 0; i < MAX_OBJS_IN_PLAY; ++i)
    {
        ser_Obj(fp, objects + i);
    }
    tmpi32 = htobe32(depth);
    checked_fwrite(&tmpi32, 1, sizeof tmpi32, fp);
    ser_Player(fp, &u);
    /* Clean up */
    fflush(fp);
    fclose(fp);
    /* Compress! */
    i = system("gzip kebich.sav");
    if (i == -1)
    {
        fatal("system() returned -1");
    }
    else if (WIFEXITED(i))
    {
        if (WEXITSTATUS(i) != 0)
        {
            fatal("gzip returned non-zero exit status %d", WEXITSTATUS(i));
        }
    }
    else if (WIFSIGNALED(i))
    {
        fatal("gunzip of save file received fatal signal %d", WTERMSIG(i));
    }
    game_finished = 1;
    free(rngbuf);
    return;
}

void load_game(void)
{
    FILE *fp;
    int i;
    uint32_t *rngbuf = malloc(cc20_savesize());
    i = system("gunzip kebich.sav");
    if (i == -1)
    {
        fatal("system() returned -1");
    }
    else if (WIFEXITED(i))
    {
        if (WEXITSTATUS(i) != 0)
        {
            fatal("gunzip returned non-zero exit status %d", WEXITSTATUS(i));
        }
    }
    else if (WIFSIGNALED(i))
    {
        fatal("gunzip of save file received fatal signal %d", WTERMSIG(i));
    }
    fp = fopen("kebich.sav", "rb");
    checked_fread(rngbuf, 1, cc20_savesize(), fp);
    deser_cc20_prng(&running_generator, rngbuf);
    free(rngbuf);
    room_reset();
    build_level();
    deser_map(fp);
    for (i = 0; i < MAX_PERMON_SLOTS; ++i)
    {
        deser_Permon_stat(fp, permon_stats + i);
    }
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        deser_Permobj_stat(fp, permobj_stats + i);
    }
    for (i = 0; i < MAX_MONS_IN_PLAY; ++i)
    {
        deser_Mon(fp, monsters + i);
    }
    for (i = 0; i < MAX_OBJS_IN_PLAY; ++i)
    {
        deser_Obj(fp, objects + i);
    }
    rebuild_roomnums();
    rebuild_mapobjs();
    rebuild_mapmons();
    checked_fread(&depth, 1, sizeof depth, fp);
    deser_Player(fp, &u);
    fclose(fp);
    unlink("kebich.sav");
    status_updated = 1;
    map_updated = 1;
    print_msg("Game loaded.\n");
}

static void rnum_init(bool use_strong)
{
    uint32_t keybuf[CHACHA_KEY_WORDS];
    uint32_t noncebuf[CHACHA_NONCE_WORDS];
#ifdef USE_DEV_URANDOM
    /* On platforms where we have access to a PRNG device node, obtain a key
     * and nonce by reading data from it. */
    if (use_strong)
    {
        /* Initialize the key and nonce by reading data from the system
         * random number device. */
        int fd;
        fd = open("/dev/urandom", O_RDONLY, 0);
        if (fd != -1)
        {
            ssize_t s;
            s = read(fd, keybuf, sizeof keybuf);
            if (s != sizeof keybuf)
            {
                fatal("rnum_init: short or failed read from /dev/urandom");
            }
            s = read(fd, noncebuf, sizeof noncebuf);
            if (s != sizeof noncebuf)
            {
                fatal("rnum_init: short or failed read from /dev/urandom");
            }
        }
        close(fd);
    }
    else
#endif
    {
        /* Fallback: Initialize the libc rand() implementation from the current
         * system time and use it to populate the key and nonce buffers.  This
         * would be completely inappropriate in a network-facing application,
         * but is tolerable for a simple computer game if you don't mind people
         * being able to predict the starting states of the game. */
        int i;
        int seed;
        print_msg("NOTICE: Using weak fallback initialization for PRNG.\n");
#ifdef GETTIMEOFDAY_HAS_REAL_USEC
        {
            /* On platforms where the microseconds part of the value emitted
             * by gettimeofday actually means something, cause some mild
             * annoyance to people trying to predict starting states by
             * perturbing the seed with the microsecond part of the current
             * time. */
            struct timeval tv;
            gettimeofday(&tv, NULL);
            seed = tv.tv_sec ^ tv.tv_usec;
        }
#else
        /* The traditional bad PRNG seed: the current system time, to the
         * nearest second. */
        seed = time(NULL);
#endif
        srand(seed);
        for (i = 0; i < 8; ++i)
        {
            keybuf[i] = rand();
        }
        for (i = 0; i < 3; ++i)
        {
            noncebuf[i] = rand();
        }
    }
    init_cc20_prng(&running_generator, keybuf, noncebuf);
}

int exclusive_flat(int lower, int upper)
{
    return lower + one_die(upper - lower - 1);
}

int inclusive_flat(int lower, int upper)
{
    return lower + zero_die(upper - lower + 1);
}

int one_die(int sides)
{
    return 1 + ((sides > 1) ? invoke_cc20_prng(&running_generator) / ((CHACHA_RAND_MAX / (uint32_t) sides) + 1) : 0);
}

int zero_die(int sides)
{
    return ((sides > 1) ? invoke_cc20_prng(&running_generator) / ((CHACHA_RAND_MAX / (uint32_t) sides) + 1) : 0);
}

int dice(int count, int sides)
{
    int total = 0;
    for ( ; count > 0; count--)
    {
        total += one_die(sides);
    }
    return total;
}

void new_game(void)
{
    u_init();
    rnum_init(true);
    make_new_level();
    status_updated = 1;
    map_updated = 1;
}

bool do_command(Full_cmd *fcmd)
{
    int i;
    Passfail pf;
    bool implicit_completed = false;
    switch (fcmd->cmd)
    {
    case MOVE_NORTH:
        return move_player(-1, 0);
    case MOVE_SOUTH:
        return move_player(1, 0);
    case MOVE_EAST:
        return move_player(0, 1);
    case MOVE_WEST:
        return move_player(0, -1);
    case MOVE_NW:
        return move_player(-1, -1);
    case MOVE_NE:
        return move_player(-1, 1);
    case MOVE_SE:
        return move_player(1, 1);
    case MOVE_SW:
        return move_player(1, -1);

    case ATTACK:
        {
            Direction *s = (Direction *) fcmd->extra;
            bool r = player_attack(s->y, s->x);
            free(s);
            return r;
        }

    case GET_ITEM:
        if (mapobject[u.y][u.x] != NO_OBJ)
        {
            attempt_pickup();
            return true;
        }
        else
        {
            print_msg("Nothing to get.\n");
            return false;
        }

    case WIELD_WEAPON:
        {
            int obj = fcmd->extra;
            if (obj == NO_OBJ)
            {
                /* TODO decide whether to announce the UI bug */
                return false;
            }
            if ((u.melee_weap != obj) && (u.ranged_weap != obj))
            {
                if (obj_is_melee_weapon(obj))
                {
                    implicit_completed = (u.melee_weap == NO_OBJ) || unwield_melee();
                    return implicit_completed && wield_weapon(obj);
                }
                else if (obj_is_ranged_weapon(obj))
                {
                    implicit_completed = (u.ranged_weap == NO_OBJ) || unwield_ranged();
                    return implicit_completed && wield_weapon(obj);
                }
            }
            else
            {
                /* If you try to wield the weapon you're already wielding,
                 * nothing at all happens. */
                return false;
            }
        }
        return false;

    case WEAR_ARMOUR:
        {
            int obj = fcmd->extra;
            if (u.armour != NO_OBJ)
            {
                print_msg("You are already wearing armour. Take it off first.\n");
                return false;
            }
            return wear_armour(obj);
        }
        return false;

    case TAKE_OFF_ARMOUR:
        return take_off_armour();

    case GO_DOWN_STAIRS:
        leave_level();
        make_new_level();
        return false;

    case STAND_STILL:
        return true;

    case USE_CONSUMABLE:
        i = fcmd->extra;
        return use_consumable(u.inventory[i]);

    case REMOVE_PLUGIN:
        if (u.plugin == NO_OBJ)
        {
            print_msg("You have no plugin module to remove!\n");
            return false;
        }
        return remove_plugin();

    case PLUG_IN_PLUGIN:
        return plug_in_plugin(fcmd->extra);

    case DROP_ITEM:
        i = fcmd->extra;
        if (i >= 0)
        {
            if ((u.inventory[i] != NO_OBJ) &&
                ((u.inventory[i] == u.plugin) ||
                 (u.inventory[i] == u.armour)))
            {
                print_msg("You cannot drop something you are wearing.\n");
                return false;
            }
            pf = drop_obj(i);
            return (pf == PF_pass);
        }
        return false;

    case SAVE_GAME:
        game_finished = true;
        save_game();
        return false;

    case UNWIELD_RANGED:
        return unwield_ranged();

    case UNWIELD_MELEE:
        return unwield_melee();

    case QUIT:
        game_finished = true;
        return false;
    }
    return false;
}

void main_loop(void)
{
    Full_cmd fcmd;
    int i;
    Speed action_speed;
    print_msg("Welcome to Martin's Infinite Dungeon.\n");
    while (!game_finished)
    {
        switch (game_tick & 3)
        {
        case 0:
        case 2:
            action_speed = 0;
            break;
        case 1:
            action_speed = 1;
            break;
        case 3:
            action_speed = 2;
            break;
        }
        /* Player is always speed 1, for now. */
        if (action_speed <= 1)
        {
            bool didcmd = false;
            do
            {
                /* Take commands until the player does
                 * something that uses an action. */
                bool gotcmd = get_command(&fcmd);
                if (gotcmd)
                {
                    didcmd = do_command(&fcmd);
                }
                if (game_finished)
                {
                    break;
                }
            } while (!didcmd);
            if (game_finished)
            {
                break;
            }
        }
        for (i = 0; i < MAX_MONS_IN_PLAY; i++)
        {
            if (monsters[i].used == 0)
            {
                /* Unused monster */
                continue;
            }
            if (action_speed <= permons[monsters[i].mon_id].speed)
            {
                mon_acts(i);
            }
            if (game_finished)
            {
                break;
            }
        }
        if (game_finished)
        {
            break;
        }
        bookkeep_u();
        game_tick++;
    }
}

noreturn void fatal(char const *fmt, ...)
{
    va_list ap;
    display_shutdown(true);
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);
    _exit(1);
}

void save_rng_state(void)
{
    memcpy(&saved_generator, &running_generator, sizeof running_generator);
}

/*! \brief Hash up to four bytes of a string to a single byte */
uint8_t string_bytehash(char const *s)
{
    uint8_t val = 0;
    int i;
    for (i = 0; i < 4; ++i)
    {
        val = (((val & 0xfu) << 4) | ((val & 0xf0u) >> 4)) + *s;
        if (*s) ++s;
    }
    return val;
}

/*! \brief Hash up to four bytes of a string to a 16-bit integer */
uint16_t string_shorthash(char const *s)
{
    uint16_t val = 0;
    int i;
    for (i = 0; i < 4; ++i)
    {
        val = (((val & 0xffu) << 8) | ((val & 0xff00u) >> 8)) + *s;
        if (*s) ++s;
    }
    return val;
}

int main(void)
{
    struct stat s;
    int i;
    display_init();
    mon_init();
    obj_init();
    /* Do we have a saved game? */
    i = stat("kebich.sav.gz", &s);
    if (!i)
    {
        /* Yes! */
        print_msg("Loading...\n");
        load_game();
    }
    else
    {
        /* No! */
        new_game();
    }
    main_loop();
    display_shutdown(false);
    return 0;
}

