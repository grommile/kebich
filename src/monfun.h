/*! \file monfun.h
 * \brief monster functions header file for Kebich
 */

/* Copyright © 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_monfun_h
#define INC_monfun_h

extern void mon_acts(int mon);
extern void death_drop(int mon);
extern void print_mon_name(int mon, Article article);
extern void summon_demon_near(int y, int x);
extern int create_mon(int pm_idx, int y, int x);
extern void summoning(int y, int x, int how_many);
extern int ood(int power, int ratio) CONSTFUNC;
extern int get_random_pmon(void);
extern void damage_mon(int mon, int amount, bool by_you, int killer);
extern bool mon_can_pass(int mon, int y, int x) PUREFUNC;
extern void move_mon(int mon, int y, int x);
extern void select_space(int * restrict py, int * restrict px, int dy, int dx, AI_style style);

extern void mon_init(void);

#endif

/* monfun.h */
