/*! \file kebich.h
 * \brief main header file for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#ifndef INC_kebich_h
#define INC_kebich_h

#define PUREFUNC __attribute__((pure))
#define CONSTFUNC __attribute__((const))
#define MYUNUSED __attribute((unused))

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <stdnoreturn.h>

#ifdef linux
#define GETTIMEOFDAY_HAS_REAL_USEC
#define USE_DEV_URANDOM
#endif

/*! \brief Size limits for various pools */
enum array_sizes
{
    PLAYER_NAME_DISPLAYED = 16,
    INVENTORY_SIZE = 19,
    MAX_PERMOBJ_SLOTS = 100,
    MAX_PERMON_SLOTS = 100,
    MAX_OBJS_IN_PLAY = 100,
    MAX_MONS_IN_PLAY = 100,
    MAX_FLAVOURS = 20
};

typedef enum game_colours
{
    Gcol_red = 1,
    Gcol_green = 2,
    Gcol_dark_yellow = 3,
    Gcol_blue = 4,
    Gcol_magenta = 5,
    Gcol_cyan = 6,
    Gcol_light_grey = 7,
    Gcol_dark_grey = 8,
    Gcol_light_red = 9,
    Gcol_light_green = 10,
    Gcol_yellow = 11,
    Gcol_light_blue = 12,
    Gcol_light_magenta = 13,
    Gcol_light_cyan = 14,
    Gcol_white = 15
} Game_colour;

/*! \brief Damage type of an attack or resistance */
typedef enum damtyp {
    DT_PHYS = 0, DT_COLD, DT_FIRE, DT_NECRO, DT_ELEC
} Damtyp;

/* XXX STRUCTURES XXX */

/*! \brief time constants for the player */
enum time_constants {
    PLAYER_REGEN_INTERVAL = 20,
    SHIELD_CHIP_INTERVAL = 2,
    SHIELD_BREAK_INTERVAL = 10
};

/*! \brief Caps on statistics. */
enum player_stat_consts
{
    MAX_EXP_LEVEL = 99,
    BASE_PLAYER_HP = 20,
    PLAYER_HP_PER_LVL = 1,
    MAX_PLAYER_HP = 999,
    BASE_PLAYER_SH = 30,
    PLAYER_SH_PER_LVL = 5,
    MAX_PLAYER_SH = 999,
    BASE_PLAYER_TOHIT = 0,
    PLAYER_TOHIT_PER_LVL = 2,
    BASE_PLAYER_EVASION = 0,
    PLAYER_EVA_PER_LVL = 2,
    BASE_PLAYER_SHIELD_REGEN = 1,
    MAX_PLAYER_SHIELD_REGEN = 999
};

typedef int8_t Tinynum;
typedef int16_t Damagenum;
typedef int16_t Accuracynum;
typedef int32_t Gametimenum;
typedef int32_t Objnum;

/*! \brief The player character */
typedef struct player {
    char name[PLAYER_NAME_DISPLAYED + 1]; /*!< player character name */
    int32_t y; /*!< y-coordinate of current position */
    int32_t x; /*!< x-coordinate of current position */
    Damagenum hpmax; /*!< Max hit points; max of 999. */
    Damagenum hpcur; /*!< Current hit points; <= 0 is dead. */
    Damagenum shmax; /*!< Max shield; max of 999. */
    Damagenum shcur; /*!< Current shield; if it's gone, damage affects HP. */
    Accuracynum tohit; /*!< Current accuracy. */
    Accuracynum evasion; /*!< Current evasion. */
    Gametimenum shield_timer; /*!< Next tick on which your shield is allowed to regenerate */
    Damagenum shield_regen; /*!< Shield regeneration rate in points/turn */
    Tinynum level; /*!< gain levels to get more powerful - lockstepped */
    int32_t moniez; /*!< futile metric of score  */
    Objnum inventory[INVENTORY_SIZE]; /*!< items carried */
    Objnum melee_weap; /*!< currently equipped melee weapon */
    Objnum ranged_weap; /*!< currently equipped ranged weapon */
    Objnum armour; /*!< currently equipped armour */
    Objnum plugin; /*!< currently equipped plug-in module */
} Player;

extern Player u;

#define NO_SLOT (-1)
#define INVALID_SLOT (-2)

/* XXX enum terrain_num */
/* To start with, only four types of terrain: Wall, Floor, Door, Stairs. */
typedef enum terrain_num {
    WALL = 0, FLOOR = 1, DOOR = 2, STAIRS = 3
} Terrain;

enum
{
    WORKBUF_SIZE = 128
};

/*! \brief the value designating no permon or a random permon */
#define NO_PMON (-1)

/*! \brief number of uint32_t to dedicate to permon flags */
enum {
    PMON_FLAG_FIELDS = 2
};

/*! \brief flag bits for permon flags field 0 */
enum PMFLAGS_0 {
    PMF0_wallwalk = 0x00000001u, /*!< Walks through walls */
    PMF0_undead = 0x00010000u /*!< From beyond the grave */
};

/*! \brief Action speed of an actor */
typedef enum speed {
    Speed_slow = 0, /*!< 66% of normal speed */
    Speed_normal = 1,
    Speed_fast = 2 /*!< 133% of normal speed */
} Speed;

/*! \brief AI pathing algorithm to use */
typedef enum ai_style
{
    AI_charger = 0,
    AI_seek_cardinal = 1,
    AI_avoid_cardinal = 2,
    AI_supine = 3,
    AI_patrol = 4,
    AI_escaper = 5,
} AI_style;

typedef enum faction
{
    Faction_neutral = 0,
    Faction_friendly = 1,
    Faction_hostile = 2,
    Faction_betrayed = 3 // player has been naughty
} Faction;

/*! \brief constant baseline data about a type of monster */
typedef struct permon {
    bool valid; /*!< This entry actually contains something */
    const char name_en[32]; /*!< Name of the monster type in English localization */
    const char plural_en[32]; /*!< Plural form of the monster's name in English localization */
    char sym; /*!< ASCII representation of the monster */
    int32_t rarity; /*!< Percentage chance of being rejected on a monster generation roll */
    int32_t power; /*!< Power level, used to govern monster generation */
    Damagenum hp; /*!< Base hit points */
    Accuracynum tohit; /*!< to-hit bonus */
    Damagenum dam; /*!< melee damage die sides */
    Accuracynum evasion; /*!< evasion bonus */
    Speed speed;
    AI_style ai;
    Faction faction;
    uint32_t flags[PMON_FLAG_FIELDS];
} Permon;

/*! \brief Gameplay statistics about a permon */
typedef struct permon_stat {
    uint32_t num_generated;
    uint32_t killed_by_player;
    uint32_t killed_by_other;
} Permon_stat;

typedef enum death {
    DEATH_KILLED, DEATH_KILLED_MON
} Death;

/*! \brief the value designating no permobj or a random permobj */
#define NO_POBJ (-1)

enum poclass_num {
    POCLASS_NONE = 0, POCLASS_WEAPON = 1, POCLASS_ARMOUR = 2,
    POCLASS_PLUGIN = 3, POCLASS_CONSUMABLE = 4, POCLASS_SHINIES = 5
};

/*! \brief number of uint32_t to dedicate to permon flags */
enum {
    POBJ_FLAG_FIELDS = 2
};

/*! \brief flag bits for permobj flags field 0 */
enum POFLAGS_0 {
    POF0_advanced   = 0x00000001u, /*!< advanced technology */
    POF0_easyknown  = 0x00000002u, /*!< is always known */
    POF0_ranged     = 0x00000100u, /*!< weapon is ranged */
    POF0_stackable  = 0x00010000u, /*!< is stackable */
    POF0_currency   = 0x00020000u, /*!< worthless scoremoniez */
};

/*! \brief lookup numbers for effects of useable items */
typedef enum use_effect {
    EFFECT_nil = -1,
    EFFECT_restore_shield, //!< restores potency%, rounded up
    EFFECT_boost_shield, //!< +potency; ignores shield cap; xs decay 1/turn
    EFFECT_restore_health, //!< restores potency%, rounded up
    EFFECT_monster_sense, //!< sees all nearby monsters; lasts potency ticks
} Use_effect;

/*! \brief structure containing functional properties of permobjs */
typedef struct objpower {
    int32_t damage; //!< Damage per attack
    int32_t sec_dmg; //!< secondary damage per attack
    Damtyp sec_dtyp; //!< type of secondary damage
    int32_t atk_rate; //!< Attacks per attack action
    int32_t shield; //!< Bonus to max shields
    int32_t shield_regen; //!< Bonus to shield regen
    int32_t evasion; //!< Bonus to evasion percentage
    int32_t tohit; //!< Bonus to attack accuracy
    Use_effect effect; //!< Effect index for consumables
    int32_t potency; //!< Potency of consumable effect
} Objpower;

/*! \brief static data about a type of object */
typedef struct permobj {
    bool valid; /*!< validity flag */
    const char name_en[32]; /*!< English-language name, used as lookup key */
    const char plural_en[32]; /*!< English-language plural. */
    char sym; /*!< Display symbol */
    char const *description_en; /*!< English-language descriptive text. */
    enum poclass_num poclass; /*!< Class of permobj. */
    int32_t mindepth; /*!< Minimum creation depth. */
    int32_t rarity; /*!< Chance in 100 of being thrown away and regen'd. */
    uint32_t dollarval;
    Objpower power;
    uint32_t flags[POBJ_FLAG_FIELDS];
} Permobj;

typedef struct permobj_stat {
    uint32_t num_generated;
    bool identified;
    int16_t flavour; /*!< only applicable to randomized types */
} Permobj_stat;

/*! \brief data about an instantiated monster */
typedef struct mon {
    int32_t mon_id;
    int32_t y;
    int32_t x;
    bool used;
    int16_t hpmax;
    int16_t hpcur;
    int16_t tohit;
    int16_t evasion;
    int16_t dam;
    Faction current_faction;
    bool awake;
} Mon;

/*! \brief Value indicating "not a monster" */
#define NO_MON (-1)

/*! \brief data about an instantiated object */
typedef struct obj {
    int32_t obj_id; /*!< permobj index */
    int32_t quan; /*!< How many? Most items don't stack. */
    bool with_you; /*!< in player inventory */
    int32_t y; /*!< y coordinate if not carried */
    int32_t x; /*!< x coordinate if not carried */
    bool used; /*!< Entry is occupied. */
} Obj;

/*! \brief Value indicating "not an object" */
#define NO_OBJ (-1)

/*! \brief "Rogue value" for x/y coordinates */
#define NO_POS (INT_MIN)

/*! \brief Article specifier for pretty-printing names */
typedef enum article
{
    Art_indef,
    Art_def,
    Art_indef_cap,
    Art_def_cap
} Article;

typedef enum passfail
{
    PF_pass = 0,
    PF_fail = -1,
} Passfail;

typedef enum game_cmd {
    MOVE_WEST, MOVE_SOUTH, MOVE_NORTH, MOVE_EAST, MOVE_NW, MOVE_NE, MOVE_SW,
    MOVE_SE, GO_DOWN_STAIRS, STAND_STILL,
    GET_ITEM, DROP_ITEM, USE_CONSUMABLE,
    WIELD_WEAPON, UNWIELD_MELEE, UNWIELD_RANGED,
    WEAR_ARMOUR, TAKE_OFF_ARMOUR,
    PLUG_IN_PLUGIN, REMOVE_PLUGIN,
    ATTACK,
    QUIT, SAVE_GAME
} Game_cmd;

typedef struct full_cmd {
    Game_cmd cmd;
    intptr_t extra;
} Full_cmd;

typedef struct direction {
    int32_t y;
    int32_t x;
} Direction;
/*! \brief Minimum x/y size of the "camera" */
#define CAMERA_SIZE 21
/*! \brief x/y dimension of level */
#define DUN_SIZE 42
/*! \brief not in a room */
#define NO_ROOM (-1)

/*! \brief nature of linkage between rooms */
typedef enum roomlinkage {
    Roomlink_none = 0, /*!< rooms not linked */
    Roomlink_direct, /*!< rooms directly connected to each other */
    Roomlink_indirect, /*!< rooms indirectly connected to each other */
    Roomlink_self /*!< room is connected to itself because it is itself */
} Roomlinkage;

/*! \brief room geometry constants */
enum room_geometry {
    ROOMS_PER_ROW = 3,
    ROOMS_PER_COLUMN = 3
};

/*! \brief Number of rooms in a level */
#define MAX_ROOMS ((ROOMS_PER_ROW) * (ROOMS_PER_COLUMN))

typedef struct roombound {
    int32_t top_y;
    int32_t bot_y;
    int32_t left_x;
    int32_t right_x;
} Roombound;

/*! \brief Mapcell state flags */
enum mapflags {
    MAPFLAG_EXPLORED = 0x00000001u /*!< this square has been explored */
};

typedef bool (*Inv_filter) (int obj, intptr_t extra);

typedef struct i32list {
    int len;
    int32_t *list;
} I32list;

/* no data before this point */
/********************************** DATA *************************************/
/* no types past this point */

/* permobjs and permons */
extern Permobj const permobjs[MAX_PERMOBJ_SLOTS];
extern Permobj_stat permobj_stats[MAX_PERMOBJ_SLOTS];
extern struct permon const permons[MAX_PERMON_SLOTS];
extern Permon_stat permon_stats[MAX_PERMON_SLOTS];
extern int last_populated_permobj;
extern int last_populated_permon;

/* objects and monsters */
extern struct obj objects[MAX_OBJS_IN_PLAY];
extern struct mon monsters[MAX_MONS_IN_PLAY];

extern bool game_finished;
extern int32_t game_tick;

/* The game map */
extern int mapobject[DUN_SIZE][DUN_SIZE];
extern int mapmonster[DUN_SIZE][DUN_SIZE];
extern Terrain terrain[DUN_SIZE][DUN_SIZE];
extern uint32_t mapflags[DUN_SIZE][DUN_SIZE];
extern int8_t roomnums[DUN_SIZE][DUN_SIZE];
extern Roombound roombounds[MAX_ROOMS];
extern int32_t dun_max_y;
extern int32_t dun_max_x;
extern int32_t dun_min_y;
extern int32_t dun_min_x;
extern int32_t depth;

/* flags for telling the display subsystem what to redraw */
extern bool status_updated;
extern bool map_updated;
extern bool display_initialized;

/* No functions before this point. */
/****************************** FUNCTIONS ************************************/
/* No data past this point. */

extern int player_attack(int dy, int dx);
extern int mhitu(int mon);
extern int uhitm(int mon);
extern int mshootu(int mon, enum damtyp dtype);
extern int ushootm(int sy, int sx);

extern int read_input(char *buffer, int length);
extern void print_msg(const char *fmt, ...);
extern void print_help(void);
extern int display_init(void);
extern void display_update(void);
extern int display_shutdown(bool skip_prompt);
extern void print_inv(Inv_filter filter, intptr_t filter_param);
extern int inv_select(char const *action, Inv_filter filter, intptr_t filter_param);
extern bool get_command(Full_cmd *dest);
extern int select_dir(int *psy, int *psx);
extern int getyn(const char *msg);
extern int get_player_name(char *buf, int max_printables);

/* various notification messages
 *
 * This is an effort to move print_msg() calls *out* of random bits and pieces
 * of objects.c, monsters.c, etc. */
extern void notify_used_last(int po);
extern void notify_wield(int obj);
extern void notify_wear(int obj);
extern void notify_plug_in(int obj);
extern void notify_unwield(int obj);
extern void notify_ready_ranged(int obj);
extern void notify_take_off(int obj);
extern void notify_remove(int obj);
extern void notify_use_useless(void);
extern void notify_shield_broken(void);
extern void notify_ukillm(int mon);
extern void notify_mkillm(int er, int ee);
extern void notify_mondie(int mon);
extern void notify_death(Death d, char const *what);
extern void notify_now_have(int slot);
extern void notify_get(int obj);

/* "I've changed things that need to be redisplayed" flags. */
extern int exclusive_flat(int lower, int upper); /* l+1 ... u-1 */
extern int inclusive_flat(int lower, int upper); /* l ... u */
extern int one_die(int sides);  /* 1..n */
extern int dice(int count, int sides);
extern int zero_die(int sides); /* 0..n-1 */
extern void save_rng_state(void);
extern noreturn void fatal(char const *fmt, ...);

extern uint8_t string_bytehash(char const *s);
extern uint16_t string_shorthash(char const *s);
static inline void i32list_init(I32list *list)
{
    list->len = 0;
    list->list = NULL;
}
static inline void i32list_append_uc(I32list *list, int32_t i)
{
    list->len++;
    list->list = realloc(list->list, list->len * sizeof (int32_t));
    list->list[list->len - 1] = i;
}

/*! \brief a random percentage value for testing N-percent chances */
static inline int percentile(void) { return zero_die(100); }

/*! \brief a random percentage value from nothing to everything */
static inline int full_percent(void) { return zero_die(101); }

/*! \brief an unweighted coin toss */
static inline bool coinflip(void) { return zero_die(2); }

extern bool tile_in_sight(int32_t y, int32_t x) PUREFUNC;

/*! \brief lower of two 16-bit integers */
static inline int16_t min_int16(int16_t l, int16_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 16-bit integers */
static inline int16_t max_int16(int16_t l, int16_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two 32-bit integers */
static inline int32_t min_int32(int32_t l, int32_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 32-bit integers */
static inline int32_t max_int32(int32_t l, int32_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two 64-bit integers */
static inline int64_t min_int64(int64_t l, int64_t r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two 64-bit integers */
static inline int64_t max_int64(int64_t l, int64_t r)
{
    return (l > r) ? l : r;
}

/*! \brief lower of two integers */
static inline int min_int(int l, int r)
{
    return (l < r) ? l : r;
}

/*! \brief higher of two integers */
static inline int max_int(int l, int r)
{
    return (l > r) ? l : r;
}

/*! \brief sign of an integer */
static inline int mysign(int a)
{
    return (a > 0) ? 1 : ((a < 0) ? -1 : 0);
}

/*! \brief absolute value of a signed integer */
static inline int myabs(int a)
{
    return (a >= 0) ? a : -a;
}

static inline int geom_dist_from_orthogonal(int dy, int dx) CONSTFUNC;
static inline int geom_dist_from_diagonal(int dy, int dx) CONSTFUNC;

/*! \brief distance of (dy,dx) from an orthogonal */
static inline int geom_dist_from_orthogonal(int dy, int dx)
{
    return min_int(myabs(dy), myabs(dx));
}

/*! \brief distance of (dy,dx) from a diagonal */
static inline int geom_dist_from_diagonal(int dy, int dx)
{
    return myabs(myabs(dy) - myabs(dx));
}

static inline bool geom_is_cardinal(int dy, int dx)
{
    return (dy == 0) || (dx == 0) || (myabs(dy) == myabs(dx));
}

extern int geom_towards_diagonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
extern int geom_towards_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
extern int geom_towards_orthogonal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);
extern int geom_flee_goal(int dy, int dx, int *restrict new_dy, int *restrict new_dx, int max_points);

/*! \brief Validity check of a coordinate pair */
static inline bool pos_is_nowhere(int32_t y, int32_t x)
{
    return (y == NO_POS) || (x == NO_POS);
}

/*! \brief bounds check of a coordinate pair */
static inline bool pos_is_oob(int32_t y, int32_t x)
{
    return (y < dun_min_y) || (x < dun_min_x) || (y > dun_max_y) || (x >= dun_max_x);
}

/*! \brief magnitude of a coordinate pair under chebyshev */
static inline int32_t cheby_magnitude(int32_t y, int32_t x)
{
    return max_int32(myabs(y), myabs(x));
}

#endif

/* kebich.h */
