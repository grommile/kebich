/*! \file objects.c
 * \brief Object handling for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define OBJECTS_C
#include "kebich.h"
#include "objfun.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "ufun.h"
#include <stdlib.h>
#include <string.h>

Obj objects[MAX_OBJS_IN_PLAY];
int get_random_pobj(void);

static I32list pobjlut[256];
static I32list pocltables[POCLASS_SHINIES + 1];

static void pobjlut_register(int po)
{
    uint8_t hash = string_bytehash(permobjs[po].name_en);
    i32list_append_uc(pobjlut + hash, po);
}

static void pocltab_register(int po)
{
    enum poclass_num pocl = permobjs[po].poclass;
    i32list_append_uc(pocltables + pocl, po);
}

/*! \brief Consume an object
 *
 * Consumes one object from the object entry referred to by obj.
 *
 * \return true if the entry is now vacant or obj was a bad index
 * \todo implement some kind of alert
 * \param obj index of object to consume
 */
bool consume_obj(int obj)
{
    Obj *optr;
    int i;
    if (obj > MAX_OBJS_IN_PLAY)
    {
        print_msg("\n\nBUG: invalid object index %d > %d\npassed to consume_obj()!\n\n", obj, MAX_OBJS_IN_PLAY);
        return true;
    }
    if (obj < 0)
    {
        print_msg("\n\nBUG: invalid object index %d < 0\npassed to consume_obj()!\n\n", obj);
        return true;
    }
    optr = objects + obj;
    if (!optr->used)
    {
        print_msg("\n\nBUG: consume_obj() called on already-released object.\n\n");
        return true;
    }
    if (optr->quan < 1)
    {
        print_msg("\n\nBUG: consume_obj() called on object with current quantity %d < 1\n\n", optr->quan);
        optr->used = false;
        return true;
    }
    --(optr->quan);
    if (optr->quan < 1)
    {
        if (optr->with_you)
        {
            for (i = 0; i <INVENTORY_SIZE; ++i)
            {
                if (u.inventory[i] == obj)
                {
                    u.inventory[i] = NO_OBJ;
                }
            }
        }
        optr->used = false;
        return true;
    }
    return false;
}

int create_obj_class(enum poclass_num po_class, int quantity, int with_you, int y, int x)
{
    int po_idx;
    int tryct;
    if (po_class == POCLASS_NONE)
    {
        po_idx = -1;
    }
    else if (pocltables[po_class].len == 0)
    {
        return NO_OBJ;
    }
    else
    {
        for (tryct = 0; tryct < 200; tryct++)
        {
            po_idx = pocltables[po_class].list[zero_die(pocltables[po_class].len)];
            if (percentile() < permobjs[po_idx].rarity)
            {
                continue;
            }
            break;
        }
    }
    return create_obj(po_idx, quantity, with_you, y, x);
}

int get_random_pobj(void)
{
    int tryct;
    int po_idx;
    for (tryct = 0; tryct < 200; tryct++)
    {
        po_idx = zero_die(last_populated_permobj + 1);
        if (percentile() < permobjs[po_idx].rarity)
        {
            po_idx = NO_POBJ;
            continue;
        }
        break;
    }
    return po_idx;
}

int create_obj(int po_idx, int quantity, int with_you, int y, int x)
{
    int i;
    for (i = 0; i < MAX_OBJS_IN_PLAY; i++)
    {
        if (!objects[i].used)
        {
            break;
        }
    }
    if (i == MAX_OBJS_IN_PLAY)
    {
        print_msg("ERROR: Ran out of objects[].\n");
        return NO_OBJ;
    }
    if (po_idx == NO_POBJ)
    {
        po_idx = get_random_pobj();
        if (po_idx == NO_POBJ)
        {
            return NO_OBJ;
        }
    }
    objects[i].obj_id = po_idx;
    if (with_you)
    {
        int slot = get_free_inventory_slot();
        if (slot == NO_SLOT)
        {
            /* inventory full; create object at player's feet instead */
            with_you = false;
            y = u.y;
            x = u.x;
        }
        else
        {
            u.inventory[slot] = i;
            objects[i].with_you = with_you;
            y = NO_POS;
            x = NO_POS;
        }
    }
    objects[i].used = 1;
    objects[i].y = y;
    objects[i].x = x;
    if (permobjs[po_idx].flags[0] & POF0_currency)
    {
        objects[i].quan = dice(depth + 1, 20);
    }
    else
    {
        objects[i].quan = quantity;
    }
    if (!objects[i].with_you)
    {
        mapobject[y][x] = i;
    }
    return i;
}

void print_obj_name(int obj)
{
    Obj *optr;
    Permobj const *poptr;
    optr = objects + obj;
    poptr = permobjs + optr->obj_id;
    if (optr->quan > 1)
    {
        print_msg("%d %s", optr->quan, poptr->plural_en);
    }
    else
    {
        print_msg("a %s", poptr->name_en);
    }
}

Passfail drop_obj(int inv_idx)
{
    Obj *optr;
    bool implicit_completed;
    optr = objects + u.inventory[inv_idx];
    if (mapobject[u.y][u.x] == NO_OBJ)
    {
        optr->y = u.y;
        optr->x = u.x;
        mapobject[u.y][u.x] = u.inventory[inv_idx];
        if (u.melee_weap == u.inventory[inv_idx])
        {
            implicit_completed = unwield_melee();
            if (!implicit_completed)
            {
                print_msg("You can't unwield that to drop it.\n");
                return PF_fail;
            }
        }
        else if (u.ranged_weap == u.inventory[inv_idx])
        {
            implicit_completed = unwield_ranged();
            if (!implicit_completed)
            {
                print_msg("You can't unwield that to drop it.\n");
                return PF_fail;
            }
        }
        u.inventory[inv_idx] = NO_OBJ;
        optr->with_you = 0;
        print_msg("You drop ");
        print_obj_name(mapobject[u.y][u.x]);
        print_msg(".\n");
        return PF_pass;
    }
    else
    {
        print_msg("There is already an item here.\n");
    }
    return PF_fail;
}

void attempt_pickup(void)
{
    int i;
    int stackable;
    int obj = mapobject[u.y][u.x];
    int po;

    if (obj != NO_OBJ)
    {
        po = objects[obj].obj_id;
        if (permobjs[po].flags[0] & POF0_currency)
        {
            int32_t d = objects[obj].quan * permobjs[po].dollarval;
            u.moniez += d;
            objects[mapobject[u.y][u.x]].used = false;
            mapobject[u.y][u.x] = NO_OBJ;
            map_updated = true;
            print_msg("You collect currency worth %d dollars.\n", d);
            return;
        }
        stackable = po_is_stackable(po);
        if (stackable)
        {
            for (i = 0; i < INVENTORY_SIZE; i++)
            {
                if ((objects[u.inventory[i]].obj_id == objects[obj].obj_id))
                {
                    notify_get(obj);
                    objects[u.inventory[i]].quan += objects[obj].quan;
                    objects[obj].used = 0;
                    mapobject[u.y][u.x] = NO_OBJ;
                    notify_now_have(i);
                    return;
                }
            }
        }
        i = get_free_inventory_slot();
        if (i == NO_SLOT)
        {
            print_msg("Your pack is full.\n");
            return;
        }
        u.inventory[i] = obj;
        mapobject[u.y][u.x] = NO_OBJ;
        objects[obj].with_you = true;
        objects[obj].x = NO_POS;
        objects[obj].y = NO_POS;
        notify_now_have(i);
    }
}

/*! \brief Mark a permobj as identified
 *
 * \todo decide whether to permanently inline this or flesh it out
 */
void identify_pobj(int po)
{
    permobj_stats[po].identified = true;
}

/*! \brief Standard inventory filter, selecting a single permobj class */
bool classic_inv_filter(int obj, intptr_t extra)
{
    int pocl = (int) extra;
    return (obj != NO_OBJ) && ((pocl == POCLASS_NONE) || (pocl_of_obj(obj) == pocl));
}

int get_pobj_by_name(char const *name_en)
{
    uint8_t hash = string_bytehash(name_en);
    int i;
    if (pobjlut[hash].len == 0)
    {
        return NO_POBJ;
    }
    for (i = 0; i < pobjlut[hash].len; ++i)
    {
        if (!strcmp(name_en, permobjs[pobjlut[hash].list[i]].name_en))
        {
            return pobjlut[hash].list[i];
        }
    }
    return NO_POBJ;
}

void obj_init(void)
{
    int i;
    for (i = 0; i < MAX_PERMOBJ_SLOTS; ++i)
    {
        if (!permobjs[i].valid)
        {
            last_populated_permobj = i - 1;
            break;
        }
        else
        {
            pobjlut_register(i);
            pocltab_register(i);
        }
    }
    return;
}

/* objects.c */
