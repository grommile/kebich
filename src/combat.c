/*! \file combat.c
 * \brief Combat routines for Kebich
 */

/* Copyright © 2005, 2016 Martin Read
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "kebich.h"
#include "pobjfun.h"
#include "monfun.h"
#include "pmonfun.h"
#include "ufun.h"

int player_attack(int dy, int dx)
{
    if (mapmonster[u.y + dy][u.x + dx] != NO_MON)
    {
        uhitm(mapmonster[u.y + dy][u.x + dx]);
    }
    else if (u.ranged_weap != NO_OBJ)
    {
        ushootm(dy, dx);
    }
    else
    {
        print_msg("Nothing to attack.\n");
        return 0;
    }
    return 1;
}

int uhitm(int mon)
{
    Mon *mp;
    Obj *wep;
    Permobj const *pwep;
    int hits = 0;
    int tohit;
    int damage;
    int atk_cnt;
    wep = objects + u.melee_weap;
    pwep = permobjs + wep->obj_id;
    mp = monsters + mon;
    for (atk_cnt = 0; atk_cnt < pwep->power.atk_rate; ++atk_cnt)
    {
        tohit = percentile() + u.tohit;
        if (tohit < mp->evasion)
        {
            print_msg("You miss.\n");
            continue;
        }
        print_msg("You hit ");
        print_mon_name(mon, 1);
        print_msg(".\n");
        if (u.melee_weap != NO_OBJ)
        {
            damage = one_die(pwep->power.damage);
            if (pwep->power.sec_dmg > 0)
            {
                // TODO check victim resistances.
                damage += one_die(pwep->power.sec_dmg);
            }
        }
        else
        {
            damage = 1;
        }
        print_msg("You do %d damage.\n", damage);
        damage_mon(mon, damage, true, NO_MON);
        if (!monsters[mon].used)
        {
            break;
        }
    }
    return hits;   /* Hit. */
}

int ushootm(int sy, int sx)
{
    /* Propagate a missile in direction (sy,sx). Attack first target in
     * LOF. */
    int tohit;
    int range;
    int y, x;
    int done = 0;
    int atk_cnt;
    Mon *mptr;
    Obj *wep;
    Permobj const *pwep;
    int damage;
    int hits = 0;
    wep = objects + u.ranged_weap;
    pwep = permobjs + wep->obj_id;
    for (atk_cnt = 0; atk_cnt < pwep->power.atk_rate; ++atk_cnt)
    {
        damage = one_die(pwep->power.damage);
        y = u.y + sy;
        x = u.x + sx;
        range = 1;
        for ( ; !done; (y += sy), (x += sx))
        {
            if (mapmonster[y][x] != NO_MON)
            {
                done = 1;
                mptr = monsters + mapmonster[y][x];
                tohit = percentile() + mptr->tohit;
                if (range == 1)
                {
                    /* Shooting at point-blank is tricky */
                    tohit = (tohit + 1) / 2;
                }
                if (tohit >= mptr->evasion)
                {
                    print_msg("You hit ");
                    print_mon_name(mapmonster[y][x], 1);
                    print_msg(".\n");
                    damage_mon(mapmonster[y][x], damage, true, NO_MON);
                    ++hits;
                    break;
                }
                else
                {
                    print_msg("You miss ");
                    print_mon_name(mapmonster[y][x], 1);
                    print_msg(".\n");
                    break;
                }
            }
            else if ((terrain[y][x] == WALL) || (terrain[y][x] == DOOR))
            {
                print_msg("The %s absorbs your shot.\n", (terrain[y][x] == WALL) ? "wall" : "door");
                break;
            }
        }
    }
    return hits;
}

int mhitu(int mon)
{
    int tohit;
    int damage;
    Mon *mptr = monsters + mon;
    print_mon_name(mon, 3);
    tohit = percentile() + mptr->tohit;
    if (tohit < u.evasion)
    {
        print_msg(" misses you.\n");
        return 0;
    }
    damage = one_die(mptr->dam);
    print_msg(" hits you.\n");
    print_msg("You take %d damage.\n", damage);
    damage_u(damage, false, DEATH_KILLED_MON, permons[mptr->mon_id].name_en);
    display_update();
    return 1;
}

int mshootu(int mon, enum damtyp dtype)
{
    Mon *mptr;
    Mon *bystander;
    int y;
    int x;
    int dy;
    int dx;
    int sy, sx;
    int done;
    int unaffected __attribute__((unused)) = 0;
    int tohit;
    int damage;
    mptr = monsters + mon;
    y = mptr->y;
    x = mptr->x;
    /* dy, dx == trajectory of missile */
    dy = u.y - y;
    dx = u.x - x;
    sy = (dy > 0) ? 1 : ((dy < 0) ? -1 : 0);
    sx = (dx > 0) ? 1 : ((dx < 0) ? -1 : 0);
    tohit = percentile() + mptr->tohit;
    /* Move projectile one square before looking for targets. */
    for ((done = 0), (y = mptr->y + sy), (x = mptr->x + sx);
         !done;
         (y += sy), (x += sx))
    {
        if ((terrain[y][x] == WALL) || (terrain[y][x] == DOOR))
        {
            done = 1;
        }
        if ((y == u.y) && (x == u.x))
        {
            if (tohit >= u.evasion)
            {
                done = 1;
                print_msg("It hits you!\n");
                if (!unaffected)
                {
                    damage = one_die(mptr->dam);
                    print_msg("You take %d damage.\n", damage);
                    damage_u(damage, false, DEATH_KILLED_MON, permons[mptr->mon_id].name_en);
                }
                display_update();
                return 1;
            }
            else
            {
                print_msg("It misses you.\n");
            }
        }
        else if (mapmonster[y][x])
        {
            done = 1;
            bystander = monsters + mapmonster[y][x];
            switch (dtype)
            {
            case DT_COLD:
                if (pmon_resists_cold(bystander->mon_id))
                {
                    unaffected = 1;
                }
                else
                {
                    unaffected = 0;
                }
                break;
            case DT_FIRE:
                if (pmon_resists_fire(bystander->mon_id))
                {
                    unaffected = 1;
                }
                else
                {
                    unaffected = 0;
                }
                break;
            case DT_NECRO:
                if (pmon_is_undead(bystander->mon_id))
                {
                    unaffected = 1;
                }
                else
                {
                    unaffected = 0;
                }
                break;
            default:
                unaffected = 0;
                break;
            }
            if (tohit >= bystander->evasion)
            {
                damage = one_die(mptr->dam);
                damage_mon(mapmonster[y][x], dtype, false, mon);
            }
        }
    }
    return 0;
}

/* combat.c */
